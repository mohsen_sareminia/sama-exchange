<?php namespace App\Extended;

use Illuminate\View\Compilers\BladeCompiler as BC;


class BladeCompiler extends BC
{

    protected $persianTag = ['{{{{', '}}}}'];
    protected $persianRawTag = ['{{!!', '!!}}'];


    /**
     * Get the echo methods in the proper order for compilation.
     *
     * @return array
     */
    protected function getEchoMethods()
    {
        $methods = [
            'compileRawEchos' => strlen(stripcslashes($this->rawTags[0])),
            'compileEscapedEchos' => strlen(stripcslashes($this->escapedTags[0])),
            'compileRegularEchos' => strlen(stripcslashes($this->contentTags[0])),
            'compilePersianEchos' => strlen(stripcslashes($this->persianTag[0])),
            'compileRawPersianEchos' => strlen(stripcslashes($this->persianRawTag[0])),
        ];

        uksort($methods, function ($method1, $method2) use ($methods) {
            // Ensure the longest tags are processed first
            if ($methods[$method1] > $methods[$method2]) {
                return -1;
            }
            if ($methods[$method1] < $methods[$method2]) {
                return 1;
            }

            // Otherwise give preference to raw tags (assuming they've overridden)
            if ($method1 === 'compileRawEchos') {
                return -1;
            }
            if ($method2 === 'compileRawEchos') {
                return 1;
            }

            if ($method1 === 'compileEscapedEchos') {
                return -1;
            }
            if ($method2 === 'compileEscapedEchos') {
                return 1;
            }
        });

        return $methods;
    }

    protected function compilePersianEchos($value)
    {
        $pattern = sprintf('/(@)?%s\s*(.+?)\s*%s(\r?\n)?/s', $this->persianTag[0], $this->persianTag[1]);

        $callback = function ($matches) {
            $whitespace = empty($matches[3]) ? '' : $matches[3] . $matches[3];

            $wrapped = sprintf($this->echoFormat, $this->compileEchoDefaults($matches[2]));

            return $matches[1] ? substr($matches[0], 1) : '<?php echo \App\Helper\jdf::tr_num(' . $wrapped . ',"fa","."); ?>' . $whitespace;
        };

        return preg_replace_callback($pattern, $callback, $value);
    }

    protected function compileRawPersianEchos($value)
    {
        $pattern = sprintf('/(@)?%s\s*(.+?)\s*%s(\r?\n)?/s', $this->persianRawTag[0], $this->persianRawTag[1]);

        $callback = function ($matches) {
            $whitespace = empty($matches[3]) ? '' : $matches[3] . $matches[3];

            return $matches[1] ? substr($matches[0], 1) : '<?php echo \App\Helper\jdf::tr_num(' . $this->compileEchoDefaults($matches[2]) . '
            ,"fa","."); ?>' . $whitespace;
        };

        return preg_replace_callback($pattern, $callback, $value);
    }
}