<?php
/**
 * Created by PhpStorm.
 * User: mohsen
 * Date: 10/2/17
 * Time: 2:58 PM
 */

namespace App\Webservice;


class TNews
{
    const URL = 'https://tnews.ir/rss/%D8%A7%D8%B1%D8%B2';

    public static function get()
    {
        $data = simplexml_load_file(self::URL);
        $result = [];
        if (isset($data->channel) && isset($data->channel->item)) {
            $items = $data->channel->item;
            foreach ($items as $item) {
                $old_date = $item->pubDate;
                $old_date_timestamp = strtotime($old_date);
                $offset = intval(date('Z'));
                $pub_date = date('Y-m-d H:i:s', $old_date_timestamp-$offset);

                $result[] = [
                    'title' => $item->title,
                    'description' => $item->description,
                    'link' => $item->link,
                    'pub_date' => $pub_date,
                ];
            }
        }

        return $result;
    }
}