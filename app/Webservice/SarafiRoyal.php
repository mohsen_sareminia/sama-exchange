<?php
/**
 * Created by PhpStorm.
 * User: mohsen
 * Date: 9/19/17
 * Time: 8:30 PM
 */

namespace App\Webservice;


class SarafiRoyal
{
    const URL = 'http://sarafiroyal.com/userfile/rss/CoinPriceRSS.xml';

    public static function get()
    {
        $data = simplexml_load_file(self::URL);
        $result = [];
        if (isset($data->channel) && isset($data->channel->item)) {
            if (isset($data->channel->item[0])) {
                $rates = explode("/", $data->channel->item[0]->category);
                if (count($rates) >= 2) {
                    $result['gold'] = [
                        'currency' => 'gold',
                        'rate' => floatval($rates[0]),
                        'rate_buy' => floatval($rates[1]),
                    ];
                }
            }
            if (isset($data->channel->item[1])) {
                $rates = explode("/", $data->channel->item[1]->category);
                if (count($rates) >= 2) {
                    $result['gold_2'] = [
                        'currency' => 'gold_2',
                        'rate' => floatval($rates[0]),
                        'rate_buy' => floatval($rates[1]),
                    ];
                }
            }
            if (isset($data->channel->item[2])) {
                $rates = explode("/", $data->channel->item[2]->category);
                if (count($rates) >= 2) {
                    $result['gold_4'] = [
                        'currency' => 'gold_4',
                        'rate' => floatval($rates[0]),
                        'rate_buy' => floatval($rates[1]),
                    ];
                }
            }
            if (isset($data->channel->item[3])) {
                $rates = explode("/", $data->channel->item[3]->category);
                if (count($rates) >= 2) {
                    $result['gold_gr'] = [
                        'currency' => 'gold_gr',
                        'rate' => floatval($rates[0]),
                        'rate_buy' => floatval($rates[1]),
                    ];
                }
            }
        }

        return $result;
    }
}