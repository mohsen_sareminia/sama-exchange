<?php

namespace App\Webservice;

use App\Setting;

class Cbi
{
    const URL = 'http://cbi.ir/ExRatesRss.aspx';

    public static function get()
    {
        $data = simplexml_load_file(self::URL);
        $result = [];
        if (isset($data->channel) && isset($data->channel->item)) {
            $items = $data->channel->item;
            $currency = array_keys(Setting::getCurrency()->jsonDecodeValue());
            foreach ($items as $item) {
                if (in_array(strtolower($item->category), $currency)) {
                    $result[] = [
                        'currency' => strtolower($item->category),
                        'rate' => floatval($item->description),
                        'rate_buy' => floatval($item->description),
                    ];
                }
            }
        }

        return $result;
    }
}