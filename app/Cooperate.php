<?php

namespace App;

use App\Mail\CooperateMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class Cooperate extends Model
{
    protected $fillable = ['first_name', 'last_name', 'phone', 'email', 'description', 'salary', 'file'];

    public function sendMail()
    {
        $values = Setting::getByKey('values');
        $email = $values->getValueByKey('forms_email');
        try {
            Mail::to($email)->send(new CooperateMail($this));
        } catch (\Exception $e) {
            file_put_contents(storage_path('logs/cooperate_email.log'), $e->getMessage());
        }
    }

    public function uploadFile($file)
    {
        $path = storage_path('cooperate');
        $name = $this->id . '.' . $file->getClientOriginalExtension();
        if (!File::isDirectory($path)) {
            File::makeDirectory($path);
        }
        $file->move($path, $name);
        $this->update([
            'file' => $name,
        ]);
    }

    public function getFilePath()
    {
        return storage_path('cooperate/' . $this->file);
    }
}
