<?php
/**
 * Created by PhpStorm.
 * User: mohsen
 * Date: 9/21/17
 * Time: 7:58 AM
 */

namespace App\Helper;

use App\ExchangeRate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Api
{
    public static function ExchangeQuery(Request $request)
    {
        $q = ExchangeRate::query();
        if ($request->has('currency')) {
            $currency = $request->currency;
            if (is_string($currency) && $currency != '') {
                $q = $q->where('currency', $currency);
            } else if (is_array($currency) && count($currency) > 0) {
                $q = $q->whereIn('currency', $currency);
            }
        }
        if ($request->has('date_from')) {
            $v = Validator::make($request->all(), [
                'date_from' => 'required|date_format:Y-m-d',
            ]);
            if ($v->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $v->errors(),
                ]);
            }
            $q = $q->where('date', '>=', DateHelper::convertFromJalali($request->date_from . ' 00:00:00'));
        }
        if ($request->has('date_to')) {
            $v = Validator::make($request->all(), [
                'date_to' => 'required|date_format:Y-m-d',
            ]);
            if ($v->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $v->errors(),
                ]);
            }
            $q = $q->where('date', '<=', DateHelper::convertFromJalali($request->date_to . ' 23:59:59'));
        }

        return $q;
    }
}