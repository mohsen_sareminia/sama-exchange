<?php

namespace App\Helper;


class DateHelper
{
    public static function convertToJalali($date, $separator = '-', $separator2 = '/', $default = null)
    {
        $date = explode(' ', $date);
        $d = explode($separator, $date[0]);
        $result = $default;
        if (count($d) >= 3) {
            $result = jdf::gregorian_to_jalali($d[0], $d[1], $d[2], $separator2);
            if (isset($date[1])) {
                $result .= ' ' . $date[1];
            }
        }

        return $result;
    }

    public static function convertFromJalali($date, $separator = '-', $separator2 = '/', $default = null)
    {
        $date = explode(' ', $date);
        $d = explode($separator, $date[0]);
        $result = $default;
        if (count($d) >= 3) {
            $result = jdf::jalali_to_gregorian($d[0], $d[1], $d[2], $separator2);
            if (isset($date[1])) {
                $result .= ' ' . $date[1];
            }
        }

        return $result;
    }
}