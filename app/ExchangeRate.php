<?php

namespace App;

use App\Helper\DateHelper;
use App\Helper\jdf;
use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $fillable = ['currency', 'rate', 'rate_buy', 'diff', 'diff_buy', 'date', 'fresh'];

    public $timestamps = false;

    public static function chart($currency = 'usd', $date_from = null, $date_to = null)
    {
        $time = time();
        $q = self::selectRaw('MAX(DATE_FORMAT(`date`, \'%Y/%m/%d\')) as `date`,MAX(`currency`) as `currency`,AVG(`rate`) as `rate`,AVG(`rate_buy`) as `rate_buy`')
            ->where(function ($q) {
                $q->where('rate', '<>', 0)->where('rate_buy', '<>', 0);
            })
            ->where('currency', strtolower($currency))
            ->groupBy(\DB::raw('YEAR(`date`), MONTH(`date`), DAY(`date`)'));
        if ($date_from) {
            $q = $q->where('date', '>=', $date_from);
        } else {
            $q = $q->where('date', '>=', date('Y-m-d 00:00:00', $time - (7 * 24 * 60 * 60)));
        }
        if ($date_to) {
            $q = $q->where('date', '<=', $date_to);
        } else {
            $q = $q->where('date', '<=', date('Y-m-d 23:59:59', $time));
        }
        $q = $q->get();

        return $q;
    }

    public static function normalize($data, $separator = '-')
    {
        return $data->map(function ($v) use ($separator) {
            $v['rate'] = round($v['rate'], 4);
            $v['rate_buy'] = round($v['rate_buy'], 4);
            $v['date_jalali'] = DateHelper::convertToJalali($v['date'], $separator);
            return $v;
        });
    }

    public static function chartLink($data, $width, $height)
    {
        $width = is_null($width) ? 600 : intval($width);
        $height = is_null($height) ? 270 : intval($height);
        $json = [];
        $json['datasets'] = [
            [
                'label' => "نرخ فروش",
                'backgroundColor' => 'rgb(255, 99, 132)',
                'borderColor' => 'rgb(255, 99, 132)',
                'data' => [],
                'fill' => false,
            ],
            [
                'label' => "نرخ خرید",
                'backgroundColor' => 'rgb(84, 135, 255)',
                'borderColor' => 'rgb(84, 135, 255)',
                'data' => [],
                'fill' => false,
            ],
        ];
        $json['labels'] = [];
        foreach ($data as $d) {
            $json['datasets'][0]['data'][] = $d->rate;
            $json['datasets'][1]['data'][] = $d->rate_buy;
            $json['labels'][] = $d->date_jalali;
        }
        $json = urlencode(json_encode($json));

        return url("chart/iframe/" . $width . "x" . $height . "/" . $json);
    }
}
