<?php

namespace App;

use App\Helper\DateHelper;
use Illuminate\Database\Eloquent\Model;

class ExchangeRateManual extends Model
{
    protected $fillable = ['currency', 'rate', 'rate_buy', 'date_from', 'date_to', 'permanent'];

    public static function normalize($data)
    {
        foreach ($data as $d) {
            $d->date_from_jalali = DateHelper::convertToJalali($d->date_from);
            $d->date_to_jalali = DateHelper::convertToJalali($d->date_to);
        }
        return $data;
    }
}
