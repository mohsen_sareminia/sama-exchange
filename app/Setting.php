<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['key', 'value'];

    public $timestamps = false;

    private $storage = [];

    public static function getCurrency()
    {
        $currency = Setting::where('key', 'currency')->first();
        if (!$currency) {
            $currency = Setting::create([
                'key' => 'currency',
                'value' => json_encode([
                    'usd' => 'دلار آمریکا',
                    'eur' => 'یورو اروپا',
                    'aed' => 'درهم امارات متحده عربی',
                    'try' => 'لير ترکيه',
                    'gold' => 'تمام سکه بهار آزادی',
                    'gold_2' => 'نیم سکه بهار آزادی',
                    'gold_4' => 'ربع سکه بهار آزادی',
                    'gold_gr' => 'سکه یک گرمی',
                    'gbp' => 'پوند انگليس',
                    'cad' => 'دلار کانادا',
                    'aud' => 'دلار استرالیا',
                    'sek' => 'کرون سو‌ید',
                    'nok' => 'کرون نروژ',
                    'dkk' => 'کرون دانمارک',
                    'chf' => 'فرانک سويس',
                    'inr' => 'روپيه هند',
                    'kwd' => 'دينار کويت',
                    'pkr100' => 'يکصد روپيه پاکستان',
                    'jpy100' => 'یکصد ين ژاپن',
                    'hkd' => 'دلار هنگ کنگ',
                    'omr' => 'ريال عمان',
                    'zar' => 'راند آفريقای جنوبی',
                    'rub' => 'روبل روسيه',
                    'qar' => 'ريال قطر',
                    'iqd100' => 'يکصد دينار عراق',
                    'syp' => 'لير سوريه',
                    'sar' => 'ريال سعودی',
                    'bhd' => 'دينار بحرين',
                    'sgd' => 'دلار سنگاپور',
                    'lkr10' => 'ده روپيه سريلانکا',
                    'npr100' => 'يکصد روپيه نپال',
                    'amd100' => 'يکصد درام ارمنستان',
                    'lyd' => 'دينار ليبی',
                    'cny' => 'یوان چين',
                    'thb100' => 'يکصد بات تايلند',
                    'myr' => 'رينگيت مالزی',
                    'krw1000' => 'يک هزار وون کره جنوبی',
                    'kzt100' => 'يکصد تنگه قزاقستان',
                    'afn' => 'افغانی افغانستان',
                    'byn' => 'روبل جديد بلاروس',
                    'azn' => 'منات آذربايجان',
                    'tjs' => 'سومونی تاجيکستان',
                    'vef' => 'بوليوار جديد ونزوئلا',
                ])
            ]);
        }

        return $currency;
    }


    public static function getByKey($key)
    {
        if(!isset($storage[$key]))
        {
            $storage[$key] = Setting::where('key', $key)->first();
            if (!$storage[$key]) {
                $storage[$key] = Setting::create([
                    'key' => $key,
                    'value' => json_encode([])
                ]);
            }
        }

        return $storage[$key];
    }

    public function getValueByKey($key, $default = '')
    {
        if (!$this->decoded) {
            $this->decoded = $this->jsonDecodeValue();
        }

        return isset($this->decoded[$key]) ? $this->decoded[$key] : $default;
    }

    public function jsonDecodeValue()
    {
        $v = json_decode($this->value, true);
        return is_array($v) ? $v : [];
    }
}
