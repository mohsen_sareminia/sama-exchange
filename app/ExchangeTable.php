<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeTable extends Model
{
    protected $fillable = ['title', 'active', 'data', 'order'];
    const DATA = [
        'columns' => [
            [
                'id' => '1',
                'active' => true,
                'value' => 'ارز',
            ],
            [
                'id' => '2',
                'active' => true,
                'value' => 'نماد',
            ],
            [
                'id' => '3',
                'active' => true,
                'value' => 'نرخ فروش',
            ],
            [
                'id' => '4',
                'active' => true,
                'value' => 'تغییرات',
            ],
            [
                'id' => '5',
                'active' => true,
                'value' => 'نرخ خرید',
            ],
            [
                'id' => '6',
                'active' => true,
                'value' => 'تغییرات',
            ],
            [
                'id' => '7',
                'active' => true,
                'value' => 'نمودار',
            ],
        ],
        'rows' => [
            [
                'type' => 'custom',
                'values' => [
                    [
                        'id' => '1',
                        'value' => '',
                    ],
                    [
                        'id' => '2',
                        'value' => '',
                    ],
                    [
                        'id' => '3',
                        'value' => '',
                    ],
                    [
                        'id' => '4',
                        'value' => '',
                    ],
                    [
                        'id' => '5',
                        'value' => '',
                    ],
                    [
                        'id' => '6',
                        'value' => '',
                    ],
                    [
                        'id' => '7',
                        'value' => '',
                    ],
                ],
            ],
        ],
    ];

    public function decodeData()
    {
        $this->data = json_decode($this->data, true);
        $this->data = is_array($this->data) ? $this->data : self::DATA;
        return $this->data;
    }

    public function getRowValue($i, $col_id, $default = null)
    {
        if (isset($this->data['rows']) && isset($this->data['rows'][$i]) && isset($this->data['rows'][$i]['values'])) {
            foreach ($this->data['rows'][$i]['values'] as $v) {
                if (isset($v['id']) && isset($v['value']) && $v['id'] == $col_id) {
                    return $v['value'];
                }
            }
        }
        return $default;
    }

    public function getColumnValue($arg1, $arg2, $default = '')
    {
        if (is_array($this->columns)) {
            return isset($this->columns[$arg1]) && isset($this->columns[$arg1][$arg2]) ? $this->columns[$arg1][$arg2] : $default;
        }

        return $default;
    }

    public static function getEmpty()
    {
        return new static([
            'data' => json_encode(self::DATA),
        ]);
    }
}
