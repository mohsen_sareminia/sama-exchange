<?php
namespace App\Mail;

use App\Cooperate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CooperateMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $cooperate;

    public function __construct(Cooperate $arg)
    {
        $this->cooperate = $arg;
    }

    public function build()
    {
        return $this->view('mails.cooperate.single')
            ->with([
                'data' => $this->cooperate,
            ])->attach($this->cooperate->getFilePath());
    }
}