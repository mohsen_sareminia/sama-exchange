<?php

namespace App\Http\Middleware;

use Closure;
use Validator;

class ApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $v = Validator::make($request->all(), [
            'key' => 'required|in:' . env('API_KEY'),
        ]);
        if ($v->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $v->errors(),
            ]);
        }

        return $next($request);
    }
}
