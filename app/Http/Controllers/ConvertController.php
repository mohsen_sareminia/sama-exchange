<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConvertController extends Controller
{
    public function index()
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();
        return view('front.convert', [
            'currency' => $currency,
        ]);
    }

    public function convert(Request $request)
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();
        $v = Validator::make($request->all(), [
            'source_rate' => 'required|in:' . join(',', array_keys($currency)),
            'destination_rate' => 'required|in:' . join(',', array_keys($currency)),
        ]);
        if ($v->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $v->errors(),
            ]);
        }

        $source = ExchangeRate::where('currency', $request->source_rate)->orderBy('date', 'DESC')->first();
        if (!$source) {
            return response()->json([
                'success' => false,
                'errors' => [
                    ['نرخ مبدا یافت نشد'],
                ],
            ]);
        }
        $destination = ExchangeRate::where('currency', $request->destination_rate)->orderBy('date', 'DESC')->first();
        if (!$destination) {
            return response()->json([
                'success' => false,
                'errors' => [
                    ['نرخ مفصد یافت نشد'],
                ],
            ]);
        }

        $ratio = $source->rate / $destination->rate;
        $value = $request->has('value') ? floatval($request->get('value')) * $ratio : $ratio;

        return response()->json([
            'success' => true,
            'ratio' => $ratio,
            'value' => $value,
            'source' => $source->rate,
            'destination' => $destination->rate,
        ]);
    }
}
