<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;
use Validator;

class NewsletterController extends Controller
{
    public function submit(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);
        if ($v->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $v->errors(),
            ]);
        }

        $find = Newsletter::where('email', $request->email)->first();
        if (!$find) {
            Newsletter::create([
                'email' => $request->email,
            ]);
        }

        return response()->json([
            'success' => true,
        ]);
    }
}
