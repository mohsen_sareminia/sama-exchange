<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;
use Validator
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('front.contact');
    }

    public function submit(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required|max:1000',
        ]);
        if ($v->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $v->errors(),
            ]);
        }

        $new = ContactUs::create($request->only('name', 'email', 'message'));
        $new->sendMail();

        return response()->json([
            'success' => true,
        ]);
    }
}
