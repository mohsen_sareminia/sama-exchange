<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\ExchangeRate;
use App\ExchangeRateManual;
use App\Helper\DateHelper;
use App\Helper\Functions;
use App\Helper\jdf;
use App\Mail\ContactUsMail;
use App\Webservice\Cbi;
use App\Webservice\SarafiRoyal;
use App\Webservice\TNews;
use Illuminate\Http\Request;
use Artisan;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class DebugController extends Controller
{
    public function debug()
    {
        $c = ContactUs::first();
//        dd($c);
        dd(Mail::to('mohsen.sareminia@gmail.com')->send(new ContactUsMail($c)));
    }

    public function log()
    {
        $log = shell_exec('tail -200 ' . storage_path('logs/laravel.log'));
        echo '<pre>';
        echo $log;
        echo '</pre>';
    }

    public function view()
    {
        dd(Artisan::call('view:clear'));
    }
}
