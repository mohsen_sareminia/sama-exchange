<?php

namespace App\Http\Controllers;

use App\Cooperate;
use Illuminate\Http\Request;
use Validator;

class CooperateController extends Controller
{
    public function index()
    {
        return view('front.cooperate');
    }

    public function submit(Request $request)
    {
        $v = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'description' => 'required|max:1000',
            'phone' => 'required|max:255',
            'salary' => 'required|numeric',
            'rezome' => 'required',
        ]);
        if ($v->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $v->errors(),
            ]);
        }
        $file = $request->file('rezome');
        if (is_null($file)) {
            return response()->json([
                'success' => false,
                'errors' => [
                    ['rezome' => 'فیلد رزومه الزامی است']
                ],
            ]);
        }
        $ext = $file->getClientOriginalExtension();
        if (!in_array($ext, ['doc', 'docx', 'pdf'])) {
            return response()->json([
                'success' => false,
                'errors' => [
                    ['rezome' => 'فایل رزومه نامعتبر است']
                ],
            ]);
        }

        $new = Cooperate::create($request->only('first_name', 'last_name', 'email', 'description', 'phone', 'salary'));
        $new->uploadFile($file);
        $new->sendMail();

        return response()->json([
            'success' => true,
        ]);
    }
}
