<?php

namespace App\Http\Controllers\Dashboard;

use App\ExchangeRate;
use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class ExchangeRateController extends Controller
{
    public function index()
    {
        $table = ExchangeRate::where('fresh',true)->get();
        $currency = Setting::getCurrency()->jsonDecodeValue();
        return view('dashboard.exchange_rate.index', [
            'table' => $table,
            'currency' => $currency,
        ]);
    }

}
