<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function index()
    {
        $table = Newsletter::latest()->paginate(20);
        return view('dashboard.newsletter.index', [
            'table' => $table,
        ]);
    }
}
