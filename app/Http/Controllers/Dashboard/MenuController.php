<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use App\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        $table = Menu::orderBy('order', 'ASC')->get();
        return view('dashboard.menu.index', [
            'table' => $table,
        ]);
    }

    public function single($id = '')
    {
        $single = new Menu();
        if ($id != '') {
            $single = Menu::find($id);
        }

        return view('dashboard.menu.single', [
            'single' => $single,
            'id' => $id,
        ]);
    }

    public function submit(Request $request, $id = '')
    {
        $this->validate($request, [
            'link' => 'required',
            'title' => 'required',
            'target' => 'required',
            'order' => 'required|integer',
        ]);
        $all = $request->except('_token');

        if ($id == '') {
            Menu::create($all);
        } else {
            Menu::where('id', $id)->update($all);
        }

        return redirect()->action('Dashboard\MenuController@index')->with('success', 1);
    }

    public function delete($id)
    {
        Menu::destroy($id);

        return redirect()->action('Dashboard\MenuController@index');
    }
}
