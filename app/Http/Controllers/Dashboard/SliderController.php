<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function index()
    {
        $table = Slider::orderBy('order', 'ASC')->get();
        return view('dashboard.slider.index', [
            'table' => $table,
        ]);
    }

    public function single($id = '')
    {
        $single = new Slider();
        if ($id != '') {
            $single = Slider::find($id);
        }

        return view('dashboard.slider.single', [
            'single' => $single,
            'id' => $id,
        ]);
    }

    public function submit(Request $request, $id = '')
    {
        $this->validate($request, [
            'link' => 'present',
            'title' => 'required',
            'target' => 'required',
            'image' => 'required',
            'order' => 'required|integer',
        ]);
        $all = $request->except('_token');

        if ($id == '') {
            Slider::create($all);
        } else {
            Slider::where('id', $id)->update($all);
        }

        return redirect()->action('Dashboard\SliderController@index')->with('success', 1);
    }

    public function delete($id)
    {
        Slider::destroy($id);

        return redirect()->action('Dashboard\SliderController@index');
    }
}
