<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function getProfile()
    {
        $user = Auth::user();
        return view('dashboard.profile.profile', [
            'user' => $user,
        ]);
    }

    public function postProfile(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ]
        ]);

        $user->update($request->only('first_name', 'last_name', 'email'));

        return redirect()->action('Dashboard\ProfileController@getProfile');
    }

    public function getPassword()
    {
        return view('dashboard.profile.password');
    }

    public function postPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed',
            'old_password' => 'required',
        ]);

        $user = Auth::user();
        if(!Hash::check($request->old_password,$user->password))
        {
            return redirect()->action('Dashboard\ProfileController@getPassword')->withErrors([
                'کلمه عبور فعلی اشتباه وارد شده است'
            ]);
        }

        $user->password = bcrypt($request->password);
        $user->save();


        return redirect()->action('Dashboard\ProfileController@getPassword');
    }

}
