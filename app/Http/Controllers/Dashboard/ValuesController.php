<?php

namespace App\Http\Controllers\Dashboard;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ValuesController extends Controller
{
    public function index()
    {
        $values = Setting::getByKey('values');
        return view('dashboard.values.index', [
            'values' => $values,
        ]);
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'values' => 'required|array'
        ]);
        $values = Setting::getByKey('values');
        $values->value = json_encode($request->values);
        $values->save();

        return redirect()->action('Dashboard\ValuesController@index');
    }
}
