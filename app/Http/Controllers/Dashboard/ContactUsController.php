<?php

namespace App\Http\Controllers\Dashboard;

use App\ContactUs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {
        $table = ContactUs::orderBy('created_at','DESC')->paginate(15);

        return view('dashboard.contact_us.index', [
            'table' => $table,
        ]);
    }

    public function delete($id)
    {
        ContactUs::destroy($id);

        return redirect()->action('Dashboard\ContactUsController@index');
    }
}
