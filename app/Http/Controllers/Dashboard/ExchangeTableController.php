<?php

namespace App\Http\Controllers\Dashboard;

use App\ExchangeRate;
use App\Http\Controllers\Controller;

use App\ExchangeTable;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ExchangeTableController extends Controller
{
    public function index()
    {
        $table = ExchangeTable::orderBy('order','ASC')->paginate(20);
        return view('dashboard.exchange_table.index', [
            'table' => $table,
        ]);
    }

    public function single($id = '')
    {
        $single = ExchangeTable::getEmpty();
        if ($id != '') {
            $single = ExchangeTable::find($id);
        }
        $currency = Setting::getCurrency()->jsonDecodeValue();

        return view('dashboard.exchange_table.single', [
            'single' => $single,
            'id' => $id,
            'currency' => $currency,
        ]);
    }

    public function submit(Request $request, $id = '')
    {
        $this->validate($request, [
            'data' => 'required',
            'order' => 'required|numeric',
        ]);
        $all = $request->except('_token');
        $all['active'] = isset($all['active']);

        if ($id == '') {
            ExchangeTable::create($all);
        } else {
            ExchangeTable::where('id', $id)->update($all);
        }

        return redirect()->action('Dashboard\ExchangeTableController@index')->with('success', 1);
    }

    public function delete($id)
    {
        ExchangeTable::destroy($id);

        return redirect()->action('Dashboard\ExchangeTableController@index');
    }
}
