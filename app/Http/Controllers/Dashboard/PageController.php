<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PageController extends Controller
{
    public function index()
    {
        $table = Page::paginate(20);
        return view('dashboard.page.index', [
            'table' => $table,
        ]);
    }

    public function single($id = '')
    {
        $single = new Page();
        if ($id != '') {
            $single = Page::find($id);
        }

        return view('dashboard.page.single', [
            'single' => $single,
            'id' => $id,
        ]);
    }

    public function submit(Request $request, $id = '')
    {
        $this->validate($request, [
            'link' => [
                'required',
                Rule::unique('pages')->ignore($id),
            ]
        ]);
        $all = $request->except('_token');
        $all['active'] = isset($all['active']);

        if ($id == '') {
            Page::create($all);
        } else {
            Page::where('id', $id)->update($all);
        }

        return redirect()->action('Dashboard\PageController@index')->with('success', 1);
    }

    public function delete($id)
    {
        Page::destroy($id);

        return redirect()->action('Dashboard\PageController@index');
    }
}
