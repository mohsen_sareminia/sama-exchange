<?php

namespace App\Http\Controllers\Dashboard;

use App\ExchangeRate;
use App\Helper\DateHelper;
use App\Http\Controllers\Controller;

use App\ExchangeRateManual;
use App\Setting;
use Illuminate\Http\Request;
use Artisan;

class ExchangeRateManualController extends Controller
{
    public function index()
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();
        $table = ExchangeRateManual::paginate(20);
        ExchangeRateManual::normalize($table->items());
        return view('dashboard.exchange_rate_manual.index', [
            'table' => $table,
            'currency' => $currency,
        ]);
    }

    public function single($id = '')
    {
        $single = new ExchangeRateManual();
        $currency = Setting::getCurrency()->jsonDecodeValue();
        if ($id != '') {
            $single = ExchangeRateManual::find($id);
        }
        $single->date_from_jalali = DateHelper::convertToJalali($single->date_from, '-', '-');
        $single->date_to_jalali = DateHelper::convertToJalali($single->date_to, '-', '-');

        return view('dashboard.exchange_rate_manual.single', [
            'single' => $single,
            'id' => $id,
            'currency' => $currency,
        ]);
    }

    public function submit(Request $request, $id = '')
    {
        $this->validate($request, [
            'currency' => 'required',
            'rate' => 'required|numeric',
            'rate_buy' => 'required|numeric',
            'date_from' => 'required|date_format:Y-m-d H:i:s',
            'date_to' => 'required|date_format:Y-m-d H:i:s',
        ]);
        $all = $request->except('_token');
        $all['date_from'] = DateHelper::convertFromJalali($all['date_from']);
        $all['date_to'] = DateHelper::convertFromJalali($all['date_to']);
        $all['permanent'] = isset($all['permanent']);

        if ($id == '') {
            ExchangeRateManual::create($all);
        } else {
            ExchangeRateManual::where('id', $id)->update($all);
        }

        $fresh = ExchangeRate::where([
            'currency' => $all['currency'],
            'fresh' => 1,
        ])->whereBetween('date', [$all['date_from'], $all['date_to']])->first();
        if ($fresh) {
            $time = time();
            $exchangeRate = \App\ExchangeRate::selectRaw('AVG(`rate`) as `rate`,AVG(`rate_buy`) as `rate_buy`,MAX(`currency`) as `currency`')
                ->where('currency', $all['currency'])
                ->whereBetween('date', [
                    date('Y-m-d H:i:00', $time - (24 * 60 * 60)),
                    date('Y-m-d H:i:00', $time),
                ])
                ->groupBy('currency')->get()->keyBy('currency');

            if (isset($exchangeRate[$all['currency']])) {
                $fresh->diff = $all['rate'] - $exchangeRate[$all['currency']]->rate;
                $fresh->diff_buy = $all['rate_buy'] - $exchangeRate[$all['currency']]->rate_buy;
            } else {
                $fresh->diff = 0;
                $fresh->diff_buy = 0;
            }
            $fresh->rate = $all['rate'];
            $fresh->rate_buy = $all['rate_buy'];
            $fresh->save();
        }

        return redirect()->action('Dashboard\ExchangeRateManualController@index')->with('success', 1);
    }

    public function delete($id)
    {
        ExchangeRateManual::destroy($id);

        return redirect()->action('Dashboard\ExchangeRateManualController@index');
    }

    public function requestWebservice()
    {
        Artisan::call('exchange_rate');
        return redirect()->action('Dashboard\ExchangeRateManualController@index');
    }
}
