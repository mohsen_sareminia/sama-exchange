<?php

namespace App\Http\Controllers\Dashboard;

use App\Cooperate;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use File;

class CooperateController extends Controller
{
    public function index()
    {
        $table = Cooperate::orderBy('created_at', 'DESC')->paginate(15);

        return view('dashboard.cooperate.index', [
            'table' => $table,
        ]);
    }

    public function delete($id)
    {
        Cooperate::destroy($id);

        return redirect()->action('Dashboard\CooperateController@index');
    }

    public function download($id)
    {
        $single = Cooperate::find($id);
        abort_if(!$single, 404);
        if (File::isFile($single->getFilePath())) {
            return response()->download($single->getFilePath());
        }
        abort(404);
    }
}
