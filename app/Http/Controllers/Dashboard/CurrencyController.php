<?php

namespace App\Http\Controllers\Dashboard;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    public function index()
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();

        return view('dashboard.currency.index', [
            'currency' => $currency,
        ]);
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'value' => 'required|array',
        ]);

        $currency = Setting::where('key', 'currency')->first();
        abort_if(!$currency, 500);

        $value = json_decode($currency->value, true);
        foreach ($request->value as $k => $v) {
            if (isset($value[$k])) {
                $value[$k] = $v;
            }
        }
        $currency->update([
            'value' => json_encode($value)
        ]);

        return redirect('dashboard/currency');
    }
}
