<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use App\ExchangeTable;
use App\Helper\Functions;
use App\Setting;
use App\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $rates = ExchangeRate::where('fresh', 1)->get()->keyBy('currency');
        $currency = Setting::getCurrency()->jsonDecodeValue();
        $slider = Slider::orderBy('order', 'ASC')->get();
        $tables = ExchangeTable::where('active', 1)->orderBy('order', 'ASC')->get();
        $tables = $tables->map(function ($t) {
            $t->decodeData();
            return $t;
        });

        $usd = ExchangeRate::chart('usd')->keyBy('date');
        $eur = ExchangeRate::chart('eur')->keyBy('date');
        $ratio = [];
        foreach ($usd as $date => $u) {
            if (isset($eur[$date])) {
                $ratio[] = [
                    'date' => $date,
                    'rate' => $u->rate / $eur[$date]->rate,
                    'rate_buy' => $u->rate_buy / $eur[$date]->rate_buy,
                ];
            }
        }
        $ratio = ExchangeRate::normalize(collect($ratio), '/');

        return view('front.home', [
            'rates' => $rates,
            'currency' => $currency,
            'slider' => $slider,
            'tables' => $tables,
            'ratio' => $ratio,
        ]);
    }
}
