<?php

namespace App\Http\Controllers\Api;

use App\ExchangeRate;
use App\Helper\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExchangeRateController extends Controller
{
    public function daily(Request $request)
    {
        $q = Api::ExchangeQuery($request)->selectRaw('MAX(DATE_FORMAT(`date`, \'%Y/%m/%d\')) as `date`,MAX(`currency`) as `currency`,AVG(`rate`) as `rate`,AVG(`rate_buy`) as `rate_buy`')
            ->groupBy(\DB::raw('YEAR(`date`), MONTH(`date`), DAY(`date`)'));
        $data = ExchangeRate::normalize($q->get(), '/');
        $chart = ExchangeRate::chartLink($data, $request->chart_width, $request->chart_height);

        return response()->json([
            'success' => true,
            'data' => $data,
            'chart' => $chart,
        ]);
    }

    public function detail(Request $request)
    {
        $q = Api::ExchangeQuery($request);
        $data = ExchangeRate::normalize($q->get(), '-');
        $chart = ExchangeRate::chartLink($data, $request->chart_width, $request->chart_height);

        return response()->json([
            'success' => true,
            'data' => $data,
            'chart' => $chart,
        ]);
    }

    public function fresh(Request $request)
    {
        $q = Api::ExchangeQuery($request)->where('fresh', 1, '-');
        $data = ExchangeRate::normalize($q->get());

        return response()->json([
            'success' => true,
            'data' => $data,
        ]);
    }
}
