<?php

namespace App\Http\Controllers;

use App\ExchangeRate;
use App\Helper\DateHelper;
use App\Setting;
use Illuminate\Http\Request;
use Validator;

class ChartController extends Controller
{
    public function index($arg)
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();
        abort_if(!isset($currency[$arg]), 404);
        return view('front.chart', [
            'currency_fa' => $currency[$arg],
            'currency_en' => $arg,
        ]);
    }

    public function history()
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();
        return view('front.chart', [
            'history' => true,
            'currency' => $currency,
        ]);
    }

    public function plot(Request $request, $arg)
    {
        $currency = Setting::getCurrency()->jsonDecodeValue();
        abort_if(!isset($currency[$arg]), 404);

        $date_from = null;
        $date_to = null;
        if ($request->has('date_from')) {
            $v = Validator::make($request->all(), [
                'date_from' => 'required|date_format:Y-m-d',
            ]);
            if ($v->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $v->errors(),
                ]);
            }
            $date_from = $request->get('date_from');
        }
        if ($request->has('date_to')) {
            $v = Validator::make($request->all(), [
                'date_to' => 'required|date_format:Y-m-d',
            ]);
            if ($v->fails()) {
                return response()->json([
                    'success' => false,
                    'errors' => $v->errors(),
                ]);
            }
            $date_to = $request->get('date_to');
        }
        $diff = date_diff(
            date_create($date_to),
            date_create($date_from)
        );
        if (abs($diff->m) > 6) {
            return response()->json([
                'success' => false,
                'errors' => [
                    'احتلاف دو تاریخ نباشد بیش از 6 ماه باشد'
                ],
            ]);
        }

        $data = ExchangeRate::chart($arg, DateHelper::convertFromJalali($date_from . ' 00:00:00'), DateHelper::convertFromJalali($date_to . ' 23:59:59'));
        $data = ExchangeRate::normalize($data, '/');

        return response()->json([
            'success' => true,
            'data' => $data,
            'currency_fa' => $currency[$arg],
            'currency_en' => $arg,
        ]);
    }

    public function iframe($width, $height, $json)
    {
        $json = json_decode(urldecode($json), true);
        if (!is_array($json)) {
            return 'not a valid json';
        }
        $json['width'] = $width;
        $json['height'] = $height;
        $json['datasets'] = isset($json['datasets']) && is_array($json['datasets']) ? $json['datasets'] : [];
        $json['labels'] = isset($json['labels']) && is_array($json['labels']) ? $json['labels'] : [];

        return view('front.chart_iframe', [
            'json' => $json,
        ]);
    }
}
