<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        return view('front.index');
    }
    public function about()
    {
        return view('front.about');
    }
    public function currency()
    {
        return view('front.currency');
    }
    public function cooperate()
    {
        return view('front.cooperate');
    }
    public function contact()
    {
        return view('front.contact');
    }
    public function news()
    {
        return view('front.news');
    }
}
