<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index($link)
    {
        $page = Page::where('link', $link)->first();
        abort_if(!$page, 404);

        return view('front.page', [
            'page' => $page,
        ]);
    }
}
