<?php

namespace App\Http\Controllers;

use App\News;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    public function index()
    {
        $count = Setting::getByKey('values')->getValueByKey('news_count', 20);
        $table = News::orderBy('pub_date', 'DESC')->paginate($count);

        return view('front.news', [
            'table' => $table,
        ]);
    }

    public function table(Request $request)
    {
        if ($request->has('per_page')) {
            $count = intval($request->per_page);
        } else {
            $count = 20;
        }
        $table = News::orderBy('pub_date', 'DESC')->paginate($count);

        return response()->json([
            'success' => true,
            'table' => $table,
        ]);
    }
}
