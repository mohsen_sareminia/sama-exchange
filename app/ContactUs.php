<?php

namespace App;

use App\Mail\ContactUsMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class ContactUs extends Model
{
    protected $fillable = ['name', 'email', 'message'];
    protected $table = 'contact_us';

    public function sendMail()
    {
        $values = Setting::getByKey('values');
        $email = $values->getValueByKey('forms_email');
        try {
            Mail::to($email)->send(new ContactUsMail($this));
        } catch (\Exception $e) {
            file_put_contents(storage_path('logs/contact_us_email.log'), $e->getMessage());
        }
    }
}
