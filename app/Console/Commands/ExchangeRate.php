<?php

namespace App\Console\Commands;

use App\ExchangeRateManual;
use App\Webservice\Cbi;
use App\Webservice\SarafiRoyal;
use Illuminate\Console\Command;
use App\Helper\jdf;

class ExchangeRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchange_rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = time();
        $date = date('Y-m-d H:i:00', $time);
        $manualRates = ExchangeRateManual::where('date_from', '<=', date('Y-m-d H:i:00', $time))
            ->where('date_to', '>=', date('Y-m-d H:i:00', $time))->get()->keyBy('currency');
        $permanentRates = ExchangeRateManual::where('permanent', 1)->get()->keyBy('currency');
        $exchangeRate = \App\ExchangeRate::selectRaw('AVG(`rate`) as `rate`,AVG(`rate_buy`) as `rate_buy`,MAX(`currency`) as `currency`')
            ->whereBetween('date', [
                date('Y-m-d H:i:00', $time - (24 * 60 * 60)),
                date('Y-m-d H:i:00', $time),
            ])
            ->groupBy('currency')->get()->keyBy('currency');
        $cbiRates = Cbi::get();
        $sarafiRoyal = SarafiRoyal::get();
        $data = array_merge($cbiRates, $sarafiRoyal);
        foreach ($data as &$r) {
            $r['date'] = $date;
            $r['fresh'] = 1;
            if (isset($manualRates[$r['currency']])) {
                if ($manualRates[$r['currency']]->rate > 0) {
                    $r['rate'] = $manualRates[$r['currency']]->rate;
                }
                if ($manualRates[$r['currency']]->rate_buy > 0) {
                    $r['rate_buy'] = $manualRates[$r['currency']]->rate_buy;
                }
            } else if (isset($permanentRates[$r['currency']])) {
                $r['rate'] = 0;
                $r['rate_buy'] = 0;
            }
            if (isset($exchangeRate[$r['currency']])) {
                $r['diff'] = $r['rate'] - $exchangeRate[$r['currency']]->rate;
                $r['diff_buy'] = $r['rate_buy'] - $exchangeRate[$r['currency']]->rate_buy;
            } else {
                $r['diff'] = 0;
                $r['diff_buy'] = 0;
            }
        }

        \App\ExchangeRate::where('fresh', 1)->update(['fresh' => 0]);
        \App\ExchangeRate::insert($data);
    }
}
