<?php

namespace App\Console\Commands;

use App\Webservice\TNews;
use Illuminate\Console\Command;

class News extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = TNews::get();
        foreach ($data as $d) {
            $count = \App\News::where('link', $d['link'])->count();
            if ($count == 0) {
                \App\News::create($d);
            }
        }
    }
}
