<html dir="rtl">
<head>
    <style>
        body {
            color: #000;
            direction: rtl;
            text-align: right;
        }

        h1 {
            font-size: 16px;
        }

        p {
            font-size: 14px;
        }
    </style>
</head>
<body>
<h1>
    نام
</h1>
<p>
    {{ $data->first_name }}
</p>
<h1>
    نام خانوادگی
</h1>
<p>
    {{ $data->last_name }}
</p>
<h1>
    ایمیل
</h1>
<p>
    {{ $data->email }}
</p>
<h1>
    تلفن
</h1>
<p>
    {{ $data->phone }}
</p>
<h1>
    حقوق
</h1>
<p>
    {{ $data->salary }}
</p>
<h1>
    تخصص
</h1>
<p>
    {{ $data->description }}
</p>
</body>
</html>