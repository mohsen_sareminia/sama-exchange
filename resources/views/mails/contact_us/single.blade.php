<html dir="rtl">
<head>
    <style>
        body {
            color: #000;
            direction: rtl;
            text-align: right;
        }

        h1 {
            font-size: 16px;
        }

        p {
            font-size: 14px;
        }
    </style>
</head>
<body>
<h1>
    نام
</h1>
<p>
    {{ $contact_us->name }}
</p>
<h1>
    ایمیل
</h1>
<p>
    {{ $contact_us->email }}
</p>
<h1>
    پیام
</h1>
<p>
    {!! str_replace(PHP_EOL,'<br>',$contact_us->message) !!}
</p>
</body>
</html>