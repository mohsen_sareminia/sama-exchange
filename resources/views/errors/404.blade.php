@extends('layouts.front')

@section('body')
    <?php $values = \App\Setting::getByKey('values'); ?>
    <div class="container">
        <div class="card">
            <div class="card-content" style="padding-top: 50px;padding-bottom: 50px;">
                <div class="row text-center">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2">
                        <h1 style="font-size: 80px">404</h1>
                        <p style="font-size: 20px">
                            صفحه مورد نظر یافت نشد
                        </p>
                        <p style="font-size: 14px;margin-top: 40px;position: relative;top: 30px">
                            آدرس:
                            <span style="direction: ltr;">{{ $values->getValueByKey('address') }}</span>
                        </p>
                        <p style="font-size: 14px;margin-top: 0;position: relative;top: 30px">
                            شماره تماس:
                            <span dir="ltr" style="direction: ltr;">{{ $values->getValueByKey('phone1') }}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop