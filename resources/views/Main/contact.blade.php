@include('/Main/top')
@yield('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row title">
                <h2>اطلاعات تماس </h2>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="context">
                        <div class="con-item">
                            <span>+98-54649876 </span><i class="fa fa-phone"></i>
                        </div>
                        <div class="con-item">
                            <span>+98-54649877 </span> <i class="fa fa-fax"></i>
                        </div>
                        <div class="con-item">
                            <span>info@samaexchange.ir </span> <i class="fa fa-envelope"></i>
                        </div>
                        <div class="con-item">
                            <span>@samaexchange</span> <i class="fa fa-send"></i>
                        </div>
                        <div class="con-item">
                            <span> میرداماد نرسیده به میدان مادر پلاک 59  </span> <i class="fa fa-building"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row title">
                <h2>فرم تماس </h2>
            </div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="sentence-text">
                        این اطلاعات مستقیما به دست مدیرعامل می رسد
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row bottom-space">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    <form class="contact-form" method="POST" action="/">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" class="form-control" name="name" placeholder="نام خود را وارد کنید" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input type="email" class="form-control" name="email" placeholder="ایمیل خود را وارد کنید" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-text-width"></i>
                                </div>
                                <textarea class="form-control" name="msg" rows="10" type="text" placeholder="پیام خود را وارد کنید"></textarea>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">تایید</button>
                    </form>
                </div>
                <div class="col-md-4">
                    <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6475.291389987276!2d51.43662299545005!3d35.75951227031518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e041111da0ad5%3A0x8088e6038f97066c!2sMirdamad+Exchange!5e0!3m2!1sen!2sir!4v1504975707308"
                            width="" heigh "600" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-2"></div>
            </div>


        </div>
    </div>

@include('/Main/footer')