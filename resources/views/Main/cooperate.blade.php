@include('/Main/top')

@yield('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="cootext">
                        با سلام و احترام خدمت شما متقاضی محترم خواهشمند است جهت ارسال درخواست خود به منظور همکاری با صرافی صما، فرم زیر را به دقت تکمیل فرمایید.

                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row title">
                <h2>فرم همکاری</h2>
            </div>
            <div class="row bottom-space">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form method="POST" action="cooperate">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input type="text" class="form-control" name="name" placeholder="نام خود را وارد کنید" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-group"></i>
                                </div>
                                <input type="text" class="form-control" name="surname" placeholder="نام خانوادگی خود را وارد کنید" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input type="" class="form-control" name="tel" placeholder="تلفن همراه خود را وارد کنید" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-inbox"></i>
                                </div>
                                <input type="email" class="form-control" name="email" placeholder="ایمیل خود را وارد کنید" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-asterisk"></i>
                                </div>
                                <textarea class="form-control" name="specific" rows="3" type="text" placeholder="تخصص های خود را وارد کنید"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-dollar"></i>
                                </div>
                                <input type="" class="form-control" name="salary" placeholder="حقوق درخواستی خود را وارد کنید (ریال)" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-file-pdf-o"></i>
                                </div>
                                <input type="file" class="form-control" name="upload" placeholder="فایل رزومه خود را آپلود کنید" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">تایید</button>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </form>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>

    </div>




@include('/Main/footer')
