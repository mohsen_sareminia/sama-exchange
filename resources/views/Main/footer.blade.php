<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> صفحات اصلی </h3>
                    <ul>
                        <li> <a href="#"> صفحه اصلی </a> </li>
                        <li> <a href="#"> درباره ‌ما </a> </li>
                        <li> <a href="#"> اخبار </a> </li>
                        <li> <a href="#"> سوابق نرخ ارز </a> </li>
                        <li> <a href="#">تماس با ما  </a> </li>
                        <li> <a href="#"> همکاری با ما</a> </li>
                    </ul>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">



                    <h3> شبکه‌های اجتماعی </h3>
                    <div style="width:80%;margin:0 auto;margin-left:30%;">
                        <ul class="social">
                            <li>
                                <a href="#"> <i class=" fa fa-facebook">   </i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-twitter">   </i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-google-plus"></i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-pinterest">   </i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-youtube">   </i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    <h3> عضویت در خبرنامه </h3>
                    <ul class="news-sender">
                        <li>
                            <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary" type="button">عضویت در خبرنامه</button>
                                        </span>
                                <input type="text" class="form-control" placeholder="ایمیل خود را وارد کنید">
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.footer-->

    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> Copyright © samaexchange 2017. All right reserved. </p>
        </div>
    </div>
    <!--/.footer-bottom-->
</footer>
</body>

<script src="{{ asset('Main_pages/js/jquery.min.js') }}"></script>
<script src="{{ asset('Main_pages/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('Main_pages/js/scripts.js') }}"></script>
</body>

</html>