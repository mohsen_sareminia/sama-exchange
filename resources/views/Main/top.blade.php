
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>صرافی صما</title>

    <meta name="description" content="صرافی صما">
    <meta name="author" content="Mhqapandaran@gmail.com">

    <link href="{{ asset('Main_pages/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('Main_pages/css/bootstrap-rtl.min.css') }}">
    <link href="{{ asset('Main_pages/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('Main_pages/css/font-awesome.min.css') }}">

</head>

<body>

<body>
<nav class="navbar navbar-default navbar" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            <ul class="nav navbar-nav navbar-right">
                <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ route('main_page') }}">صفحه اصلی</a></li>
                <li class="{{ Request::is('about*') ? 'active' : '' }}"><a href="{{ route('about_page') }}">درباره ما</a></li>
                <li class="{{ Request::is('news*') ? 'active' : '' }}"><a href="{{ route('news_page') }}">اخبار</a></li>
                <li class="{{ Request::is('currency*') ? 'active' : '' }}"><a href="{{ route('currency_page') }}">سوابق نرخ ارز</a></li>
                <li class="{{ Request::is('contact*') ? 'active' : '' }}"><a href="{{ route('contact_page') }}">تماس با ما</a></li>
                <li class="{{ Request::is('cooperate*') ? 'active' : '' }}"><a href="{{ route('cooperate_page') }}">همکاری با ما</a></li>
            </ul>
            <div class="logo" style="width:80px;float:left;">
                <img src="{{ asset('Main_pages/img/sama.png') }}" width="100%" alt="">
            </div>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-collapse -->
</nav>
