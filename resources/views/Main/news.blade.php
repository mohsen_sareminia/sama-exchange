@include('/Main/top')

@yield('content')
<div class="content">
    <div class="container-fluid">
        <div class="row title">
            <h2>اخبار</h2>
        </div>

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            دلار از مرز 3 هزار و 900 تومان گذشت/ روند صعودی ادامه دارد+ نمودار
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="/Main_pages/img/news-1.jpg" class="news-img" alt="">
                                </div>
                                <div class="col-md-9 lead">
                                    <p>
                                        روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومانروند
                                        بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 center-text"><i class="fa fa-calendar"></i> 26 شهریور</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-clock-o"></i> 12:44</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-tag"></i> بازار ارز</div>
                                    <div class="col-md-2 center-text"><button type="button" class="btn btn-fresh fleft"><a href="#">ادامه مطلب</a></button></div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            دلار از مرز 3 هزار و 900 تومان گذشت/ روند صعودی ادامه دارد+ نمودار
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="/Main_pages/img/news-1.jpg" class="news-img" alt="">
                                </div>
                                <div class="col-md-9 lead">
                                    <p>
                                        روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومانروند
                                        بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 center-text"><i class="fa fa-calendar"></i> 26 شهریور</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-clock-o"></i> 12:44</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-tag"></i> بازار ارز</div>
                                    <div class="col-md-2 center-text"><button type="button" class="btn btn-fresh fleft"><a href="#">ادامه مطلب</a></button></div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            دلار از مرز 3 هزار و 900 تومان گذشت/ روند صعودی ادامه دارد+ نمودار
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="/Main_pages/img/news-1.jpg" class="news-img" alt="">
                                </div>
                                <div class="col-md-9 lead">
                                    <p>
                                        روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومانروند
                                        بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 center-text"><i class="fa fa-calendar"></i> 26 شهریور</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-clock-o"></i> 12:44</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-tag"></i> بازار ارز</div>
                                    <div class="col-md-2 center-text"><button type="button" class="btn btn-fresh fleft"><a href="#">ادامه مطلب</a></button></div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            دلار از مرز 3 هزار و 900 تومان گذشت/ روند صعودی ادامه دارد+ نمودار
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="/Main_pages/img/news-1.jpg" class="news-img" alt="">
                                </div>
                                <div class="col-md-9 lead">
                                    <p>
                                        روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومانروند
                                        بیشترین رقم خود رسیده است.روند هفتگی نرخ دلار آزاد در هفته گذشته حاکی از آن است که در آخرین روز کاری هفته دلار با رقم ۳ هزار و ۹۰۴ تومان به بیشترین رقم خود رسیده است.
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 center-text"><i class="fa fa-calendar"></i> 26 شهریور</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-clock-o"></i> 12:44</div>
                                    <div class="col-md-2 center-text"><i class="fa fa-tag"></i> بازار ارز</div>
                                    <div class="col-md-2 center-text"><button type="button" class="btn btn-fresh fleft"><a href="#">ادامه مطلب</a></button></div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-2"></div>
        </div>


    </div>
    
</div>


@include('/Main/footer')
