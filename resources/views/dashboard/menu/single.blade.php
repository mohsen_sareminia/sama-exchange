@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\MenuController@index') }}">مدیریت منو</a>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\MenuController@single',$id) }}">
                {{ $single->id?'ویرایش':'افزودن' }}
                منو
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST" action="{{ action('Dashboard\MenuController@submit',$single->id) }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>عنوان:</label>
                                                    <input type="text" class="form-control" name="title"
                                                           value="{{ old('title') ? old('title') : $single->title }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>لینک:</label>
                                                    <input type="text" class="form-control" name="link"
                                                           value="{{ old('link') ? old('link') : $single->link }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>نوع کلیک:</label>
                                                    <select class="form-control" name="target">
                                                        <option value="_self" {{ (old('target') ? old('target') : $single->target) == '_self' ? 'selected' : '' }}>
                                                            صفحه جاری
                                                        </option>
                                                        <option value="_blank" {{ (old('target') ? old('target') : $single->target) == '_blank' ? 'selected' : '' }}>
                                                            صفحه جدید
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>اولویت:</label>
                                                    <input type="text" class="form-control" name="order"
                                                           value="{{ old('order') ? old('order') : $single->order }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent

@stop

@section('footer')
    @parent
@stop