@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\MenuController@index') }}">مدیریت منو</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            منو
                            <a class="btn btn-primary card-title-button" href="{{ action('Dashboard\MenuController@single') }}">افزودن</a>
                        </div>
                        <div class="card-block">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>عنوان</th>
                                    <th>لینک</th>
                                    <th>اولویت</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table as $row)
                                    <tr>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ $row->link }}</td>
                                        <td>{{ $row->order }}</td>
                                        <td>
                                            <a href="{{ action('Dashboard\MenuController@single',$row->id) }}"
                                               class="btn btn-outline-primary">ویرایش</a>
                                            <form class="delete-form" method="POST" action="{{ action('Dashboard\MenuController@delete',$row->id) }}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">حذف</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop