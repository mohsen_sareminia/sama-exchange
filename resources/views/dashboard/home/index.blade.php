@extends('layouts.dashboard')

@section('body')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبورد</li>
        <li class="breadcrumb-item active"><a href="{{ action('Dashboard\ChartController@index') }}">نمودار ها</a>
        </li>

        {{--<li class="breadcrumb-menu">--}}
        {{--<div class="btn-group" role="group" aria-label="Button group with nested dropdown">--}}
        {{--<a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>--}}
        {{--<a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;داشبورد</a>--}}
        {{--<a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;تنظیمات</a>--}}
        {{--</div>--}}
        {{--</li>--}}
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            نمودار ها
                            <a class="btn btn-primary card-title-button" data-toggle="modal" data-target="#chart-setting-modal">تنطیمات</a>
                        </div>
                        <div class="card-block">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('dashboard.home.modal')
@stop

@section('footer')
    @parent
    <script type="application/json" id="currency-json">{!! json_encode($currency) !!}</script>
    <script src="{{ asset('js/dashboard/home.js') }}"></script>
@stop