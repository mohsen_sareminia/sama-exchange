@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\FileController@index') }}">مدیریت واحد های پول</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST" action="{{ action('Dashboard\CurrencyController@submit') }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>
                                مدیریت واحد های پول
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    @foreach($currency as $i=>$c)
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           aria-describedby="basic-addon{{ $i }}"
                                                           name="value[{{ $i }}]"
                                                           value="{{ $c }}">
                                                    <span class="input-group-addon" id="basic-addon{{ $i }}">{{ strtoupper($i) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent
@stop