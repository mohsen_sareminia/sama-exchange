@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeRateController@index') }}">
                آخرین نرخ ها
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            آخرین نرخ ها
                        </div>
                        <div class="card-block">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>ارز</th>
                                    <th>نماد</th>
                                    <th>تاریخ بروزرسانی</th>
                                    <th>قیمت فروش</th>
                                    <th>قیمت خرید</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table as $row)
                                    <tr>
                                        <td>{{ isset($currency[$row->currency]) ? $currency[$row->currency] : '' }}</td>
                                        <td>{{ $row->currency }}</td>
                                        <td>{{ \App\Helper\DateHelper::convertToJalali($row->date) }}</td>
                                        <td>{{ $row->rate }}</td>
                                        <td>{{ $row->rate_buy }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop