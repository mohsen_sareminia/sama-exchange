@if($table->lastPage() > 1)
    <ul class="pagination">
        <?php
        $action = isset($controller) ? $controller : 'MainPageController@index';
        $actionData = isset($actionData) ? $actionData : [];
        ?>

        <li class="{{ $table->currentPage()==1?'disabled':'waves-effect' }}">
            <a rel="nofollow" href="{{ action($action,array_merge($actionData,['page'=>$table->currentPage()-1])) }}">
                <i class="fa fa-chevron-right"></i>
            </a>
        </li>
        @if($table->currentPage() > 4)
            <li class="ka-green waves-effect">
                <a rel="nofollow" href="{{ action($action,array_merge($actionData,['page'=>1])) }}">
                    1
                </a>
            </li>
            <li class="disabled">
                <a rel="nofollow">
                    ...
                </a>
            </li>
        @endif
        @for($i=$table->currentPage()-3;$i<=$table->lastPage() && $i<=($table->currentPage()+3+($table->currentPage()<4 ? 4-$table->currentPage() : 0));$i++)
            @if($i>=1)
                <li class="ka-green {{ $table->currentPage() == $i ? 'active' : '' }}">
                    <a rel="nofollow" href="{{ action($action,array_merge($actionData,['page'=>$i])) }}">{{ $i }}</a>
                </li>
            @endif
        @endfor
        @if($table->currentPage() < $table->lastPage()-4)
            <li class="disabled">
                <a rel="nofollow">
                    ...
                </a>
            </li>
            <li class="ka-green waves-effect">
                <a rel="nofollow" href="{{ action($action,array_merge($actionData,['page'=>$table->lastPage()])) }}">
                    {{ $table->lastPage() }}
                </a>
            </li>
        @endif
        <li class="{{ $table->currentPage()==$table->lastPage()?'disabled':'waves-effect' }}">
            <a rel="nofollow" href="{{ action($action,array_merge($actionData,['page'=>$table->currentPage()+1])) }}">
                <i class="fa fa-chevron-left"></i>
            </a>
        </li>

    </ul>
@endif
