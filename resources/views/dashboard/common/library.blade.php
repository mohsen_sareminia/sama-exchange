<div class="modal fade" id="library" tabindex="-1" role="dialog" aria-labelledby="libraryModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="libraryModal">کتابخانه</h4>
            </div>
            <div class="modal-body">
                <iframe src="{{ url('dashboard/elfinder/library') }}"
                        style="border: 0;width: 100%;height: 430px"></iframe>
            </div>
        </div>
    </div>
</div>

@section('header')
    @parent
    <link href="{{ asset('css/dashboard/library.css') }}" rel="stylesheet">
@stop

@section('footer')
    @parent
    <script type="text/javascript" src="{{ asset('js/dashboard/library.js') }}"></script>
@stop