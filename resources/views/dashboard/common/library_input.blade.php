<div class="input-group">
    <input type="text" class="form-control" value="{{ old($name) ? old($name) : $value }}" name="{{ $name }}">
    <span class="input-group-addon library-button" onclick="Library.open('{{ $name }}')"><i class="fa fa-image"></i></span>
</div>
<div class="library-input">
    <div class="library-input-image">
        @if(!is_null(old($name)))
            <img src="{{ asset(old($name)) }}"/>
        @elseif(!is_null($value))
            <img src="{{ asset($value) }}"/>
        @endif
    </div>
</div>