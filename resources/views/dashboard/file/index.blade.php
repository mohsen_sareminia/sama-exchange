@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\FileController@index') }}">مدیریت فایل ها</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            مدیریت فایل ها
                        </div>
                        <div class="card-block">
                            <iframe src="{{ url('dashboard/elfinder/tinymce') }}"></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent
    <style>
        iframe {
            border: 0;
            width: 100%;
            height: 450px;
        }
    </style>
@stop