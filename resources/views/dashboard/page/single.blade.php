@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\PageController@index') }}">مدیریت برگه ها</a>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\PageController@single',$id) }}">
                {{ $single->id?'ویرایش':'افزودن' }}
                برگه
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST" action="{{ action('Dashboard\PageController@submit',$single->id) }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>عنوان:</label>
                                            <input type="text" class="form-control" name="title"
                                                   value="{{ old('title') ? old('title') : $single->title }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>لینک:</label>
                                            <input type="text" class="form-control" name="link"
                                                   value="{{ old('link') ? old('link') : $single->link }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>وضعیت: </label>
                                        <label class="switch switch-icon switch-pill switch-primary-outline-alt">
                                            <input type="checkbox" class="switch-input" value="1"
                                                   {{ old('active')?'checked': (is_null($single->active)?'checked':($single->active?'checked':'')) }} name="active">
                                            <span class="switch-label" data-on="" data-off=""></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>متن: </label>
                                            <textarea class="tinymce"
                                                      name="text">{!! old('text') ? old('text') : $single->text !!}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent

@stop

@section('footer')
    @parent
    <script type="text/javascript" src="{{ asset('plugin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/init.js') }}"></script>
    <script>
        initTinyMCE();
    </script>
@stop