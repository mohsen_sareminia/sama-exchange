@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ContactUsController@index') }}">فرم تماس با ما</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            تماس با ما
                        </div>
                        <div class="card-block">
                            @foreach($table as $row)
                                <table class="table table-hover table-bordered">
                                    <tr>
                                        <td style="width: 20%">
                                            نام :
                                        </td>
                                        <td>
                                            {{ $row->name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">
                                            ایمیل :
                                        </td>
                                        <td>
                                            {{ $row->email }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">
                                            پیام :
                                        </td>
                                        <td>
                                            {!! str_replace(PHP_EOL,'<br>',$row->message) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">
                                            تاریخ ثبت :
                                        </td>
                                        <td>
                                            {{ \App\Helper\jdf::jdate('Y/m/d H:i',$row->created_at->timestamp,'','','fa') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%">
                                            عملیات :
                                        </td>
                                        <td>
                                            <form class="delete-form" method="POST"
                                                  action="{{ action('Dashboard\ContactUsController@delete',$row->id) }}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-outline-danger"
                                                        onclick="return confirm('Are you sure?')">حذف
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                </table>
                                <hr/>
                            @endforeach
                            @include('dashboard.common.pagination',['controller'=>'CooperateController@index'])
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    <style>
        .card table td {
            font-size: 12px;
        }
    </style>
@stop