<?php

return [
    [
        'title' => 'تماس با ما',
        'items' => [
            'phone1' => 'تلفن 1',
            'phone2' => 'تلفن 2',
            'email_info' => 'ایمیل',
            'address' => 'آدرس',
        ],
    ],
    [
        'title' => 'فوتر',
        'items' => [
            'working_hours' => 'ساعات کاری',
            'copyright' => 'متن کپی رایت',
        ],
    ],
    [
        'title' => 'نقشه',
        'items' => [
            'map_lat' => 'عرض جغرافیایی',
            'map_lng' => 'طول جغرافیایی',
            'map_zoom' => 'زوم نقشه',
        ],
    ],
    [
        'title' => 'شبکه اجتماعی',
        'items' => [
            'telegram' => 'تلگرام',
            'instagram' => 'اینستاگرام',
            'facebook' => 'فیسبوک',
            'google+' => 'گوگل +',
            'twitter' => 'توییتر',
            'pinterest' => 'پینترست',
            'youtube' => 'یوتیوب',
        ],
    ],
    [
        'title' => 'اپلیکیشن ها',
        'items' => [
            'app_android_link' => 'اندروید',
            'app_ios_link' => 'iOs',
        ],
    ],
    [
        'title' => 'اخبار',
        'items' => [
            'news_count' => 'تعداد خبر در هر صفحه',
        ],
    ],
    [
        'title' => 'ایمیل',
        'items' => [
            'forms_email' => 'ایمیل دریافت اطلاعات فرم ها',
        ],
    ],
];
?>