@extends('layouts.dashboard')

@section('body')
    <?php
    $items = require base_path('resources/views/dashboard/values/items.php');
    ?>
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item">
            <a href="{{ action('Dashboard\ValuesController@index') }}">
                مقادیر ثابت
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST"
                          action="{{ action('Dashboard\ValuesController@submit') }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <ul class="nav nav-pills nav-stacked" role="tablist">
                                            @foreach($items as $i=>$tab)
                                                <li role="presentation" class="{{ $i==0?'active':'' }}">
                                                    <a href="#tab{{ $i }}"
                                                       aria-controls="tab{{ $i }}"
                                                       role="tab"
                                                       data-toggle="tab">{{ $tab['title'] }}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>

                                    <div class="col-sm-9">
                                        <div class="tab-content">
                                            @foreach($items as $i=>$tab)
                                                <div role="tabpanel" class="tab-pane {{ $i==0?'active':'' }}"
                                                     id="tab{{ $i }}">
                                                    <div class="row">
                                                        @foreach($tab['items'] as $key=>$fa)
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <label>{{ $fa }}:</label>
                                                                    <input type="text" class="form-control"
                                                                           name="values[{{ $key }}]"
                                                                           value="{{ old('values['.$key.']') ? old('values['.$key.']') : $values->getValueByKey($key) }}">
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent
    <style>
        .nav>li>a {
            position: relative;
            display: block;
            padding: 10px 15px;
        }
        .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
            color: #fff;
            background-color: #337ab7;
        }
        .nav-pills>li>a {
            border-radius: 4px;
        }
        .nav>li>a:focus, .nav>li>a:hover {
            text-decoration: none;
            background-color: #eee;
        }
    </style>
@stop

@section('footer')
    @parent
    <script type="text/javascript" src="{{ asset('plugin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/init.js') }}"></script>
    <script>
        initTinyMCE();
    </script>
@stop