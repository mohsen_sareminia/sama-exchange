@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\NewsletterController@index') }}">مدیریت اعضا خبرنامه</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            اعضا خبرنامه
                        </div>
                        <div class="card-block">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>ایمیل</th>
                                    <th>تاریخ ثبت</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table as $row)
                                    <tr>
                                        <td>{{ $row->email }}</td>
                                        <td>{{ \App\Helper\jdf::jdate('Y/m/d H:i',$row->created_at->timestamp,'','','fa') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @include('dashboard.common.pagination',['controller'=>'NewsletterController@index'])
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop