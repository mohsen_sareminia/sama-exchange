@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeRateManualController@index') }}">مدیریت نرخ های دستی</a>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeRateManualController@single',$id) }}">
                {{ $single->id?'ویرایش':'افزودن' }}
                نرخ های دستی
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST"
                          action="{{ action('Dashboard\ExchangeRateManualController@submit',$single->id) }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>ارز:</label>
                                            <select class="form-control" name="currency">
                                                @foreach($currency as $i=>$c)
                                                    <option value="{{ $i }}" {{ (old('currency') ? old('title') : $single->currency)==$i?'selected':'' }}>{{ $c }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>در صورت پایان از وب سرویس نخوان: </label>
                                        <label class="switch switch-icon switch-pill switch-primary-outline-alt">
                                            <input type="checkbox" class="switch-input" value="1"
                                                   {{ old('permanent')?'checked': (is_null($single->permanent)?'checked':($single->permanent?'checked':'')) }} name="permanent">
                                            <span class="switch-label" data-on="" data-off=""></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>نرخ فروش:</label>
                                            <input type="text" class="form-control" name="rate"
                                                   value="{{ old('rate') ? old('rate') : $single->rate }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>نرخ خرید:</label>
                                            <input type="text" class="form-control" name="rate_buy"
                                                   value="{{ old('rate_buy') ? old('rate_buy') : $single->rate_buy }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>تاریخ شروع:</label>
                                            <input type="text" class="form-control datetimepicker" id="date_from" name="date_from"
                                                   value="{{ old('date_from') ? old('date_from') : $single->date_from_jalali }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>تاریخ پایان:</label>
                                            <input type="text" class="form-control datetimepicker" id="date_to" name="date_to"
                                                   value="{{ old('date_to') ? old('date_to') : $single->date_to_jalali }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent

@stop

@section('footer')
    @parent
    <script type="text/javascript" src="{{ asset('plugin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/init.js') }}"></script>
    <script>
        initTinyMCE();
    </script>
@stop