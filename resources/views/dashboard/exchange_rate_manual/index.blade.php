@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeRateManualController@index') }}">مدیریت نرخ
                های دستی</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            نرخ های دستی
                            <a class="btn btn-primary card-title-button"
                               href="{{ url('dashboard/exchange-rate-manual/single') }}">افزودن</a>
                            <form class="card-title-button" style="margin-top: 3px !important;margin-left: 5px"
                                  method="POST"
                                  action="{{ action('Dashboard\ExchangeRateManualController@requestWebservice') }}">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure?')">
                                    بروزرسانی نرخ ها
                                </button>
                            </form>
                        </div>
                        <div class="card-block">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>ارز</th>
                                    <th>نرخ</th>
                                    <th>تاریخ شروع</th>
                                    <th>تاریخ پایان</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php  ?>
                                @foreach($table as $row)
                                    <tr>
                                        <td>{{ isset($currency[$row->currency]) ? $currency[$row->currency] : '' }}</td>
                                        <td>{{ $row->rate }}</td>
                                        <td>{{ $row->date_from_jalali }}</td>
                                        <td>{{ $row->date_to_jalali }}</td>
                                        <td>
                                            <a href="{{ action('Dashboard\ExchangeRateManualController@single',$row->id) }}"
                                               class="btn btn-outline-primary">ویرایش</a>
                                            <form class="delete-form" method="POST"
                                                  action="{{ action('Dashboard\ExchangeRateManualController@delete',$row->id) }}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-outline-danger"
                                                        onclick="return confirm('Are you sure?')">حذف
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @include('dashboard.common.pagination',['controller'=>'ExchangeRateManualController@index'])
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop