@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item">
            <a href="{{ action('Dashboard\ProfileController@getProfile') }}">
                تغییر پروفایل
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST"
                          action="{{ action('Dashboard\ProfileController@postProfile') }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-2 col-md-3"></div>
                                    <div class="col-sm-8 col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>نام:</label>
                                                    <input type="text" class="form-control" name="first_name"
                                                           value="{{ old('first_name') ? old('first_name') : $user->first_name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>نام خانوادگی:</label>
                                                    <input type="text" class="form-control" name="last_name"
                                                           value="{{ old('last_name') ? old('last_name') : $user->last_name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>ایمیل:</label>
                                                    <input type="text" class="form-control" name="email"
                                                           value="{{ old('email') ? old('email') : $user->email }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent

@stop

@section('footer')
    @parent
    <script type="text/javascript" src="{{ asset('plugin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/init.js') }}"></script>
    <script>
        initTinyMCE();
    </script>
@stop