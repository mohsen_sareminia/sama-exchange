@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item">
            <a href="{{ action('Dashboard\ProfileController@getPassword') }}">
                تغییر کلمه عیور
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST"
                          action="{{ action('Dashboard\ProfileController@postPassword') }}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-2 col-md-3"></div>
                                    <div class="col-sm-8 col-md-6">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>کلمه عبور قیلی:</label>
                                                    <input type="password" class="form-control" name="old_password">
                                                </div>
                                                <div class="form-group">
                                                    <label>کلمه جدید:</label>
                                                    <input type="password" class="form-control" name="password">
                                                </div>
                                                <div class="form-group">
                                                    <label>تکرار کلمه عبور:</label>
                                                    <input type="password" class="form-control"
                                                           name="password_confirmation">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent

@stop

@section('footer')
    @parent
    <script type="text/javascript" src="{{ asset('plugin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/init.js') }}"></script>
    <script>
        initTinyMCE();
    </script>
@stop