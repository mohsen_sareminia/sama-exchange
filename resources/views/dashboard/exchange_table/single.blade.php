@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeTableController@index') }}">مدیریت جداول</a>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeTableController@single',$id) }}">
                {{ $single->id?'ویرایش':'افزودن' }}
                جدول
            </a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            @include('dashboard.common.errors')
            <div class="row">
                <div class="col-sm-12">
                    <form class="form" method="POST"
                          action="{{ action('Dashboard\ExchangeTableController@submit',$single->id) }}">
                        <input type="hidden" name="data" value="{!! $single->data !!}">
                        {{ csrf_field() }}
                        <div class="card">
                            <div class="card-header">
                                <button type="submit" class="btn btn-primary card-title-button">
                                    ثبت
                                </button>
                            </div>
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>عنوان:</label>
                                            <input type="text" class="form-control" name="title"
                                                   value="{{ old('title') ? old('title') : $single->title }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>اولویت:</label>
                                            <input type="text" class="form-control" name="order"
                                                   value="{{ old('order') ? old('order') : $single->order }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>وضعیت: </label>
                                        <label class="switch switch-icon switch-pill switch-primary-outline-alt">
                                            <input type="checkbox" class="switch-input" value="1"
                                                   {{ old('active')?'checked': (is_null($single->active)?'checked':($single->active?'checked':'')) }} name="active">
                                            <span class="switch-label" data-on="" data-off=""></span>
                                            <span class="switch-handle"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <hr/>
                                        <button class="btn btn-primary" onclick="Table.addRow()" type="button">
                                            افزودن سطر
                                        </button>
                                        <button class="btn btn-primary" onclick="Table.addCol()" type="button">
                                            افزودن ستون
                                        </button>
                                        <br/><br/>
                                        <table class="table table-hover table-striped text-center" id="output">

                                        </table>

                                        <button class="btn btn-primary" onclick="Table.addRow()" type="button">
                                            افزودن سطر
                                        </button>
                                        <button class="btn btn-primary" onclick="Table.addCol()" type="button">
                                            افزودن ستون
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop

@section('header')
    @parent
    <style>
        .currency-container label {
            width: 100%;
        }

        .remove-currency {
            color: #f00;
            float: left;
            cursor: pointer;
        }

        #output input,
        #output select {
            font-size: 12px;
        }
    </style>
@stop

@section('footer')
    @parent
    <script id="json" type="application/json">{!! $single->data !!}</script>
    <script type="text/html" id="type-input">
        <select class="form-control">
            <option value="custom">--- مقادیر دلخواه</option>
            @foreach($currency as $i=>$c)
                <option value="{{ $i }}">{{ $c }}</option>
            @endforeach
        </select>
    </script>
    <script type="text/html" id="currency-input">
        <select class="form-control">
            <option value="">انتخاب نشده</option>
            <option value="icon">آیکون</option>
            <option value="fa">ارز</option>
            <option value="en">نماد</option>
            <option value="rate">نرخ فروش</option>
            <option value="diff">تغییرات نرخ فروش</option>
            <option value="rate_buy">نرخ خرید</option>
            <option value="diff_buy">تغییرات نرخ خرید</option>
            <option value="chart">نمودار</option>
        </select>
    </script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/tinymce.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugin/tinymce/init.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/dashboard/exchange_table.js') }}"></script>
    <script>
        initTinyMCE();
    </script>
@stop