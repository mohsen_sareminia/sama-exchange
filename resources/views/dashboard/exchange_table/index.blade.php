@extends('layouts.dashboard')

@section('body')
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">داشبور</li>
        <li class="breadcrumb-item"><a href="{{ action('Dashboard\ExchangeTableController@index') }}">مدیریت جداول</a>
        </li>
    </ol>

    <div class="container-fluid">

        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-align-justify"></i>
                            جداول
                            <a class="btn btn-primary card-title-button" href="{{ url('dashboard/exchange-table/single') }}">افزودن</a>
                        </div>
                        <div class="card-block">
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th>عنوان</th>
                                    <th>اولویت</th>
                                    <th>وضعیت</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($table as $row)
                                    <tr>
                                        <td>{{ $row->title }}</td>
                                        <td>{{ $row->order }}</td>
                                        <td>{!! $row->active ? '<span class="tag tag-success">فعال</span>':'<span class="tag tag-danger">غیر فعال</span>'  !!}</td>
                                        <td>
                                            <a href="{{ action('Dashboard\ExchangeTableController@single',$row->id) }}"
                                               class="btn btn-outline-primary">ویرایش</a>
                                            <form class="delete-form" method="POST" action="{{ action('Dashboard\ExchangeTableController@delete',$row->id) }}">
                                                {{csrf_field()}}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">حذف</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                            @include('dashboard.common.pagination',['controller'=>'ExchangeTableController@index'])
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->
        </div>

    </div>
    <!--/.container-fluid-->
@stop