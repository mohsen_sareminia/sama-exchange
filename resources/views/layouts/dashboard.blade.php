<!DOCTYPE html>
<html lang="IR-fa" dir="rtl">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CoreUI Bootstrap 4 Admin Template">
    <meta name="author" content="Lukasz Holeczek">
    <meta name="keyword" content="CoreUI Bootstrap 4 Admin Template">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->
    <title>داشبورد مدیریت صما</title>
    <link rel="icon" href="{{asset('img/Logo-32.png')}}" sizes="32x32"/>
    <!-- Icons -->
    <link href="{{ asset('template/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template/css/simple-line-icons.css') }}" rel="stylesheet">
    <!-- Main styles for this application -->
    {{--<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('plugin/datetimepicker/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('template/dest/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dashboard/style.css') }}" rel="stylesheet">

    @yield('header')
</head>
<body class="navbar-fixed sidebar-nav fixed-nav">
<header class="navbar">
    <div class="container-fluid">
        <button class="navbar-toggler mobile-toggler hidden-lg-up" type="button">&#9776;</button>
        <ul class="nav navbar-nav hidden-md-down">
            <li class="nav-item">
                <a class="nav-link navbar-toggler layout-toggler" href="#">&#9776;</a>
            </li>
        </ul>
        <ul class="nav navbar-nav pull-left hidden-md-down">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button"
                   aria-haspopup="true" aria-expanded="false">
                    <img src="{{ asset('img/user.png') }}" class="img-avatar" alt="{{ Auth::user()->email }}">
                    <span class="hidden-md-down">مدیر</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <div class="dropdown-header text-xs-center">
                        <strong><i class="fa fa-gears"></i></strong>
                    </div>
                    <a class="dropdown-item" href="{{ url('dashboard/profile') }}">
                        <i class="fa fa-user"></i>
                        پروفایل
                    </a>
                    <div class="divider"></div>
                    <a class="dropdown-item" href="{{ url('dashboard/profile/password') }}">
                        <i class="fa fa-user"></i>
                        تغییر کلمه عبور
                    </a>
                    <div class="divider"></div>
                    <a class="dropdown-item" href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> خروج</a>
                </div>
            </li>
            <li class="nav-item">

            </li>

        </ul>
    </div>
</header>
<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\ContactUsController@index') }}">
                    <i class="fa fa-gears"></i>
                    فرم تماس با ما
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\CooperateController@index') }}">
                    <i class="fa fa-gears"></i>
                    فرم همکاری با ما
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\ExchangeTableController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت جداول</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\ExchangeRateManualController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت نرخ های دستی</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\ExchangeRateController@index') }}">
                    <i class="fa fa-gears"></i>
                    آخرین نرخ ها</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\CurrencyController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت واحد های پول</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\PageController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت برگه ها</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\MenuController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت منو</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\SliderController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت اسلایدر</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\NewsletterController@index') }}">
                    <i class="fa fa-gears"></i>
اعضای خبرنامه
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\ValuesController@index') }}">
                    <i class="fa fa-gears"></i>
                    مقادیر ثابت</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ action('Dashboard\FileController@index') }}">
                    <i class="fa fa-gears"></i>
                    مدیریت فایل ها</a>
            </li>
        </ul>
    </nav>
</div>
<!-- Main content -->
<main class="main">
    @yield('body')
</main>


<footer class="footer">
        <span class="text-left" style="direction: ltr;font-size: 12px">
            <a href="{{ url('/') }}" target="_blank">Sama Exchange</a> &copy; admin dashboard
        </span>
</footer>
<script>
    var HOST = '{{ url('') }}';
</script>
<!-- Bootstrap and necessary plugins -->
<script src="{{ asset('template/js/libs/jquery.min.js') }}"></script>
<script src="{{ asset('template/js/libs/tether.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/js/libs/pace.min.js') }}"></script>
<script src="{{ asset('plugin/datetimepicker/js/jalaali.js') }}"></script>
<script src="{{ asset('plugin/datetimepicker/js/script.js') }}"></script>
<script>
    $(function () {
        $.each($('.datetimepicker'), function (index, value) {
            $(value).MdPersianDateTimePicker({
                TargetSelector: '#' + $(value).attr('id'),
                Trigger: 'click',
                EnableTimePicker: true,
                Format: 'yyyy-MM-dd HH:mm:ss',
                EnglishNumber: true
            });
        });
    });
</script>


<!-- CoreUI main scripts -->

<script src="{{ asset('template/js/app.js') }}"></script>
<script src="{{ asset('js/front/script.js') }}"></script>
<script src="{{ asset('js/Request.js') }}"></script>

@yield('footer')

</body>

</html>
