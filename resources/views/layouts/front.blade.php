<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>صرافی صما</title>

    <meta name="description" content="صرافی صما">
    <meta name="author" content="Mhqapandaran@gmail.com">
    <meta name="author" content="mohsen.sareminia@gmail.com">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset('img/Logo-32.png')}}" sizes="32x32"/>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugin/toast/dist/jquery.toast.min.css') }}" rel="stylesheet">
    <link href="{{ asset('Main_pages/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    @yield('header')
</head>

<body>
<?php $menus = \App\Menu::orderBy('order', 'ASC')->get(); ?>
<?php $values = \App\Setting::getByKey('values'); ?>
<div class="logo-container">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-sm-offset-0 col-md-offset-0 col-xs-6 col-xs-offset-3">
                <img src="{{ asset('img/logo.jpg') }}" class="logo"/>
                <p class="certificate text-center">
                    با مجوز رسمی از بانک مرکزی
                </p>
                <p class="header-phone text-center">
                    تلفن :
                    {{{{ '54578' }}}}
                </p>
            </div>
            <div class="col-md-9">
                <?php

                $timezone_offset = [
                    'new_york' => \App\Helper\Functions::get_timezone_offset('America/New_York'),
                    'beijing' => \App\Helper\Functions::get_timezone_offset('Asia/Hong_Kong'),
                    'istanbul' => \App\Helper\Functions::get_timezone_offset('Asia/Istanbul'),
                    'hong_kong' => \App\Helper\Functions::get_timezone_offset('Asia/Hong_Kong'),
                    'london' => \App\Helper\Functions::get_timezone_offset('Europe/London'),
                    'dubai' => \App\Helper\Functions::get_timezone_offset('Asia/Dubai'),
                ];
                ?>
                <ul class="clocks hidden-xs">
                    <li>
                        <canvas id="clock-canvas-1" class="clock-canvas"
                                data-diff="{{ isset($timezone_offset['new_york']) ? $timezone_offset['new_york'] : '' }}"></canvas>
                        <p>
                            NEW YORK
                        </p>
                    </li>
                    <li>
                        <canvas id="clock-canvas-2" class="clock-canvas"
                                data-diff="{{ isset($timezone_offset['beijing']) ? $timezone_offset['beijing'] : '' }}"></canvas>
                        <p>
                            BEIJING
                        </p>
                    </li>
                    <li>
                        <canvas id="clock-canvas-3" class="clock-canvas"
                                data-diff="{{ isset($timezone_offset['istanbul']) ? $timezone_offset['istanbul'] : '' }}"></canvas>
                        <p>
                            ISTANBUL
                        </p>
                    </li>
                    <li>
                        <canvas id="clock-canvas-4" class="clock-canvas"
                                data-diff="{{ isset($timezone_offset['hong_kong']) ? $timezone_offset['hong_kong'] : '' }}"></canvas>
                        <p>
                            HONG KONG
                        </p>
                    </li>
                    <li>
                        <canvas id="clock-canvas-5" class="clock-canvas"
                                data-diff="{{ isset($timezone_offset['london']) ? $timezone_offset['london'] : '' }}"></canvas>
                        <p>
                            LONDON
                        </p>
                    </li>
                    <li>
                        <canvas id="clock-canvas-6" class="clock-canvas"
                                data-diff="{{ isset($timezone_offset['dubai']) ? $timezone_offset['dubai'] : '' }}"></canvas>
                        <p>
                            DUBAI
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<nav class="navbar navbar-default navbar" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <a class="menu-date" href="#">
            <ul>
                <li>
                    {{ \App\Helper\jdf::jdate('d F Y','','','','fa') }}
                </li>
                <li>
                    {{ date('d F Y') }}
                </li>
            </ul>
        </a>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            <ul class="nav navbar-nav navbar-right">
                @foreach($menus as $menu)
                    <?php
                    $requestUrl = Request::fullUrl();
                    $direct = strpos($menu->link, 'http://') !== false || strpos($menu->link, 'https://') !== false;
                    $url = $direct ? $menu->link : url($menu->link);
                    ?>
                    <li class="{{ $requestUrl==$url ? 'active' : '' }}">
                        <a href="{{ $url }}" target="{{ $menu->target }}">
                            {{{{ $menu->title }}}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-collapse -->
</nav>

@yield('body')

<footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-2  col-md-2 col-sm-4 col-xs-6">
                    <h3> صفحات اصلی </h3>
                    <ul>
                        @foreach($menus as $menu)
                            <li>
                                <a href="{{ $menu->link }}">
                                    {{{{ $menu->title }}}}
                                </a>
                            </li>
                        @endforeach
                        <li><a href="#"> اخبار </a></li>
                    </ul>
                </div>
                <div class="col-lg-2  col-md-2 col-sm-2 col-xs-6">
                    <h3> شبکه‌های اجتماعی </h3>
                    <div style="width:80%;margin:0 auto;margin-left:30%;">
                        <ul class="social">
                            <li>
                                <a href="#"> <i class=" fa fa-facebook">   </i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-twitter">   </i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-google-plus"></i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-pinterest">   </i> </a>
                            </li>
                            <li>
                                <a href="#"> <i class="fa fa-youtube">   </i> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    <h3> عضویت در خبرنامه </h3>
                    <ul class="news-sender">
                        <li>
                            <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default submit-newsletter" type="button">عضویت در خبرنامه</button>
                                        </span>
                                <input type="text" id="newsletter-email" class="form-control" placeholder="ایمیل خود را وارد کنید">
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    <h3>تماس با ما</h3>
                    <ul class="">
                        <li><i class="fa fa-building"></i>&nbsp;{{ $values->getValueByKey('address') }}</li>
                        <li><i class="fa fa-phone"></i>&nbsp;{{ $values->getValueByKey('phone1') }}</li>
                        <li><i class="fa fa-clock-o"></i>&nbsp;{{ $values->getValueByKey('working_hours') }}</li>
                    </ul>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </div>
    <!--/.footer-->

    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left">{{ $values->getValueByKey('copyright') }}</p>
        </div>
    </div>
    <!--/.footer-bottom-->
</footer>

<script>
    var HOST = '{{ url('') }}';
</script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/Request.js') }}"></script>
<script src="{{ asset('plugin/clock/clock.js') }}"></script>
<script src="{{ asset('plugin/toast/dist/jquery.toast.min.js') }}"></script>
<script src="{{ asset('Main_pages/js/scripts.js') }}"></script>
<script src="{{ asset('js/front/script.js') }}"></script>
<script src="{{ asset('js/toast.js') }}"></script>
@yield('footer')
</body>

</html>