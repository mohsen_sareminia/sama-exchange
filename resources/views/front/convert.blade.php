@extends('layouts.front')

@section('body')
    <div class="container">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="panel panel-default convert-panel">
                            <div class="panel-heading">
                                &nbsp;
                            </div>
                            <div class="panel-body">
                                <div class="form">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>مقدار:</label>
                                                <input type="text" id="value" class="form-control" value="1">
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>ارز پایه:</label>
                                                <select class="form-control" id="source">
                                                    @foreach($currency as $i=>$c)
                                                        <option value="{{ $i }}">{{ $c }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>ارز هدف:</label>
                                                <select class="form-control" id="destination">
                                                    @foreach($currency as $i=>$c)
                                                        <option value="{{ $i }}">{{ $c }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <button class="btn btn-red" onclick="search()">
                                                تبدیل
                                            </button>
                                        </div>
                                        <div class="col-md-8 col-md-offset-2 result">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('header')
    @parent
    <style>
        .result td {
            font-size: 14px;
        }

        .result td:first-child {
            width: 30%;
        }

        .convert-panel {
            margin: 30px 0;
        }
    </style>
@stop

@section('footer')
    @parent
    <script src="{{ asset('js/front/convert.js') }}"></script>
@stop