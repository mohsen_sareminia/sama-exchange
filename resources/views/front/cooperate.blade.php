@extends('layouts.front')

@section('body')
    <div class="container">
        <div class="card">
            <div class="card-content">
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <div class="cootext">
                            با سلام و احترام خدمت شما متقاضی محترم خواهشمند است جهت ارسال درخواست خود به منظور همکاری با صرافی صما، فرم زیر را به دقت تکمیل فرمایید.

                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row title">
                    <h2>فرم همکاری</h2>
                </div>
                <div class="row bottom-space">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <form>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" class="form-control" id="first_name" placeholder="نام خود را وارد کنید" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-group"></i>
                                    </div>
                                    <input type="text" class="form-control" id="last_name" placeholder="نام خانوادگی خود را وارد کنید" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="phone" class="form-control" id="phone" placeholder="تلفن همراه خود را وارد کنید" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-inbox"></i>
                                    </div>
                                    <input type="email" class="form-control" id="email" placeholder="ایمیل خود را وارد کنید" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-asterisk"></i>
                                    </div>
                                    <textarea class="form-control" id="description" rows="3" type="text" placeholder="تخصص های خود را وارد کنید"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                    <input type="number" class="form-control" id="salary" placeholder="حقوق درخواستی خود را وارد کنید (ریال)" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-file-pdf-o"></i>
                                    </div>
                                    <input type="file" class="form-control" id="rezome" placeholder="فایل رزومه خود را آپلود کنید" required>
                                </div>
                                <label style="font-size: 12px">
                                    رزومه
                                    (word یا pdf)
                                </label>
                            </div>
                            <button type="button" class="btn btn-red btn-submit">تایید</button>
                        </form>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('header')
    <link href="{{ asset('css/front/cooperate.css') }}" rel="stylesheet">
@stop

@section('footer')
    @parent
    <script src="{{ asset('js/front/cooperate.js') }}"></script>
@stop