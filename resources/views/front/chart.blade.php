@extends('layouts.front')

@section('body')
    <div class="container">
        <div class="card">
            <div class="card-content">
                @if(!isset($history) || !$history)
                    <div class="row title">
                        <h2>نمودار تغییرات نرخ {{{{ $currency_fa }}}}</h2>
                    </div>
                @endif
                @if(isset($history) && $history)
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="panel panel-default chart-panel">
                                <div class="panel-heading">
                                    &nbsp;
                                </div>
                                <div class="panel-body">
                                    <div class="form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>ارز:</label>
                                                    <select class="form-control" id="currency">
                                                        @foreach($currency as $i=>$c)
                                                            <option value="{{ $i }}">{{ $c }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" id="sell" checked> نمایش قیمت فروش
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" id="buy" checked> نمایش قیمت خرید
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>تاریخ شروع:</label>
                                                    <input type="text" class="form-control datetimepicker"
                                                           id="date_from" value="{{ \App\Helper\jdf::jdate('Y-m-d',time()-(7*24*60*60)) }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>تاریخ پایان:</label>
                                                    <input type="text" class="form-control datetimepicker" id="date_to"
                                                           value="{{ \App\Helper\jdf::jdate('Y-m-d') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button class="btn btn-red" onclick="search()">
                                                    جستجو
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                <canvas id="currency-chart"></canvas>
            </div>
        </div>
    </div>
@stop

@section('header')
    @parent
    @if(isset($currency_fa))
        <meta name="currency-fa" content="{{ $currency_fa }}"/>
    @endif
    @if(isset($currency_en))
        <meta name="currency" content="{{ $currency_en }}"/>
    @endif
    <link href="{{ asset('plugin/datetimepicker/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front/chart.css') }}" rel="stylesheet">
@stop

@section('footer')
    @parent
    <script src="{{ asset('plugin/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('plugin/datetimepicker/js/jalaali.js') }}"></script>
    <script src="{{ asset('plugin/datetimepicker/js/script.js') }}"></script>
    <script src="{{ asset('js/front/chart.js') }}"></script>
@stop