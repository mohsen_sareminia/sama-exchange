<html>
<head>
    <link href="{{ asset('plugin/datetimepicker/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front/chart.css') }}" rel="stylesheet">

</head>
<body>
<canvas id="currency-chart"></canvas>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('plugin/chartjs/Chart.bundle.min.js') }}"></script>
<script>
    var labels = JSON.parse('{!! json_encode($json['labels']) !!}');
    var datasets = JSON.parse('{!! json_encode($json['datasets']) !!}');

    var ctx = document.getElementById('currency-chart').getContext('2d');
    document.getElementById('currency-chart').height = parseInt('{{ $json['height'] }}');
    document.getElementById('currency-chart').width = parseInt('{{ $json['width'] }}');

    new Chart(ctx, {
        type: 'line',

        data: {
            labels: labels,
            datasets: datasets
        },

        options: {
            responsive: false,
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }
        }
    });

</script>
</body>
</html>
