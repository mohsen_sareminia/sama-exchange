@extends('layouts.front')

@section('body')
    <?php $values = \App\Setting::getByKey('values'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                @if ($slider->count()>0)
                    <div class="card slider-card">
                        <div class="card-content">
                            <div id="home-slider">
                                @foreach($slider as $key=>$item)
                                    <?php
                                    $direct = strpos($item->link, 'http://') !== false || strpos($item->link, 'https://') !== false;
                                    $url = $direct ? $item->link : url($item->link);
                                    ?>
                                    <div class="home-slider-image"
                                         style="background-image: url('{{url($item->image)}}')">
                                        <a class="home-slider-content"
                                           href="{{ $item->link ==''? '#':$url }}" target="{{ $item->target }}">
                                            @if($item->title != '')
                                                <div class="home-slider-overlay">
                                                    <span>{{{{ $item->title }}}}</span>
                                                </div>
                                            @endif
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-3">
                <ul class="social-network">
                    <li>
                        <a target="_blank" href="{{ $values->getValueByKey('app_android_link') }}">
                            <i class="fa fa-android"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="{{ $values->getValueByKey('app_ios_link') }}">
                            <i class="fa fa-apple"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="{{ $values->getValueByKey('telegram') }}">
                            <i class="fa fa-telegram"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="{{ $values->getValueByKey('instagram') }}">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
                <canvas id="currency-chart"></canvas>
            </div>
        </div>
        @foreach($tables as $table)
            <div class="card">
                <div class="card-content">
                    <div class="col-sm-12">
                        <h2 class="table-title">{{{{ $table->title }}}}</h2>
                    </div>
                    <table class="table table-hover table-striped text-center table-responsive currency-table">
                        <thead>
                        @foreach($table->data['columns'] as $col)
                            @if(isset($col['active']) && $col['active'] ===true)
                                <th>{{!! isset($col['value']) ? $col['value'] : $col['value'] !!}}</th>
                            @endif
                        @endforeach
                        </thead>
                        <tbody>
                        @foreach($table->data['rows'] as $i=>$row)
                            @if(isset($row['type']))
                                <tr>
                                    @foreach($table->data['columns'] as $col)
                                        @if(isset($col['id']))
                                            @if(isset($col['active']) && $col['active'] === true)
                                                <?php $value = $table->getRowValue($i, $col['id']); ?>
                                                @if($row['type'] == 'custom')
                                                    <td>{{!! $value !!}}</td>
                                                @elseif(isset($rates[$row['type']]))
                                                    @if(in_array($value,['icon']))
                                                        <td class="currency-icon-td"><img class="currency-icon" src="{{ asset('img/currency/'.strtolower($row['type'].'.png')) }}"/></td>
                                                    @elseif(in_array($value,['fa']))
                                                        <td>{{{{ isset($currency[$row['type']]) ? $currency[$row['type']] : '' }}}}</td>
                                                    @elseif(in_array($value,['en']))
                                                        <td>{{{{ strtoupper($row['type']) }}}}</td>
                                                    @elseif(in_array($value,['rate','rate_buy']))
                                                        <td>{{{{ $rates[$row['type']][$value] == 0 ? '-----' : $rates[$row['type']][$value] }}}}</td>
                                                    @elseif(in_array($value,['diff','diff_buy']))
                                                        <td>
                                                        <span class="diff">
                                            ({{{{ ($rates[$row['type']][$value] >0?'+':'').$rates[$row['type']][$value]  }}}}
                                                            )
                                        </span>
                                                            {{!! $rates[$row['type']][$value] > 0 ? '<i class="fa fa-chevron-up" aria-hidden="true"></i>' : ($rates[$row['type']][$value] < 0 ? '<i class="fa fa-chevron-down" aria-hidden="true"></i>' :'--') !!}}
                                                        </td>
                                                    @elseif(in_array($value,['chart']))
                                                        <td>
                                                            <a target="_blank" href="{{{{ url('plot/'.$row['type']) }}}}">
                                                                <i class="fa fa-line-chart" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td>{{{{ isset($currency[$row['type']]) ? $currency[$row['type']] : '' }}}}</td>
                                                    @endif
                                                @endif
                                            @endif
                                        @endif
                                    @endforeach
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @endforeach
    </div>
@stop

@section('header')
    @parent
    <link href="{{ asset('plugin/lightslider/css/lightslider.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front/chart.css') }}" rel="stylesheet">
    <link href="{{ asset('css/front/home.css') }}" rel="stylesheet">
@stop

@section('footer')
    @parent
    <script type="application/json" id="ratio-json">{!! $ratio !!}</script>
    <script src="{{ asset('plugin/lightslider/js/lightslider.min.js') }}"></script>
    <script src="{{ asset('plugin/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('js/front/home.js') }}"></script>
@stop