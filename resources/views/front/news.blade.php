@extends('layouts.front')

@section('body')
    <div class="content">
        <div class="content">
            <div class="container-fluid">
                <br/>

                @foreach($table as $row)
                    <?php $timestamp = strtotime($row->pub_date) ?>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="row">
                                <div class="panel panel-default news-panel">
                                    <div class="panel-heading">
                                        <h2>{{{{ $row->title }}}}</h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12 lead">
                                                <p>
                                                    {{{{ $row->description }}}}
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-md-offset-3 center-text news-date">
                                                <i class="fa fa-calendar"></i>
                                                {{{{ \App\Helper\jdf::jdate('d F Y',$timestamp,'','','fa') }}}}
                                            </div>
                                            <div class="col-md-3 center-text news-time">
                                                <i class="fa fa-clock-o"></i>
                                                {{{{ \App\Helper\jdf::jdate('H:i',$timestamp,'','','fa') }}}}
                                            </div>
                                            <div class="col-md-3 text-left">
                                                <a class="btn btn-red" href="{{{{ $row->link }}}}" target="_blank">
                                                    ادامه مطلب
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center">
                        @include('dashboard.common.pagination',[
                            'table'=>$table,
                            'controller'=>'NewsController@index'
                        ])
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('header')
    <link href="{{ asset('css/front/news.css') }}" rel="stylesheet">
@stop