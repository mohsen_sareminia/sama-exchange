@extends('layouts.front')

@section('body')
    <div class="container">
        <div class="card">
            <div class="card-content">
                <div class="row title">
                    <div class="col-sm-12">
                        <h2>{{{{ $page->title }}}}</h2>
                    </div>
                </div>
                <div class="row content">
                    <div class="col-sm-12">
                        {{!! $page->text !!}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('header')
    <link href="{{ asset('css/front/page.css') }}" rel="stylesheet">
@stop