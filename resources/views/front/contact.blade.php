@extends('layouts.front')
@section('body')
    <?php $values = \App\Setting::getByKey('values'); ?>
    <div class="container">
        <div class="card">
            <div class="card-content">
                <div class="row title">
                    <div class="col-sm-12">
                        <h2>اطلاعات تماس </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="con-item">
                                    <div class="con-item-inner">
                                        <i class="fa fa-phone"></i>
                                        <span dir="ltr">{{{{ $values->getValueByKey('phone1') }}}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="con-item">
                                    <div class="con-item-inner">
                                        <i class="fa fa-fax"></i>
                                        <span dir="ltr">{{{{ $values->getValueByKey('phone2') }}}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="con-item">
                                    <i class="fa fa-envelope"></i>
                                    <span>{{{{ $values->getValueByKey('email_info') }}}}</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="con-item">
                                    <i class="fa fa-send"></i>
                                    <span>
                                        <a href="https://telegram.me/{{{{ $values->getValueByKey('telegram') }}}}"
                                           target="_blank">
                                        {{{{ '@'.$values->getValueByKey('telegram') }}}}
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="con-item">
                                    <i class="fa fa-building"></i>
                                    <span> {{{{ $values->getValueByKey('address') }}}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <div class="row title">
                    <div class="col-sm-12">
                        <h2>فرم تماس </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="sentence-text">
                            این اطلاعات مستقیما به دست مدیرعامل می رسد
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row bottom-space">
                    <div class="col-md-2"></div>
                    <div class="col-md-4">
                        <form class="contact-form">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input type="text" class="form-control" id="name"
                                           placeholder="نام خود را وارد کنید"
                                           required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope"></i>
                                    </div>
                                    <input type="email" class="form-control" id="email"
                                           placeholder="ایمیل خود را وارد کنید" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-text-width"></i>
                                    </div>
                                    <textarea class="form-control" id="message" rows="10" type="text"
                                              placeholder="پیام خود را وارد کنید"></textarea>
                                </div>
                            </div>
                            <button type="button" class="btn btn-red btn-submit">
                                ارسال پیام
                            </button>
                        </form>
                    </div>
                    <div class="col-md-4">
                        <iframe class="map"
                                src="https://maps.google.com/maps?q={{ $values->getValueByKey('map_lat') }},{{ $values->getValueByKey('map_lng') }}&hl=fa;z={{ $values->getValueByKey('map_zoom') }}&amp;output=embed"
                                width="" heigh=600" frameborder="0" style="border:0;height: 361px"
                                allowfullscreen></iframe>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('header')
    <link href="{{ asset('css/front/contact.css') }}" rel="stylesheet">
@stop

@section('footer')
    @parent
    <script src="{{ asset('js/front/contact_us.js') }}"></script>
@stop