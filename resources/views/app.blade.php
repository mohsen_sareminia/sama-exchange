<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>صرافی صما</title>

    <meta name="description" content="صرافی صما">
    <meta name="author" content="Mhqapandaran@gmail.com">

    <link href="Main_pages/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="Main_pages/css/bootstrap-rtl.min.css">
    <link href="Main_pages/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="Main_pages/css/font-awesome.min.css">

</head>

<body>

<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#">صفحه اصلی</a></li>
                <li><a href="#">درباره ما</a></li>
                <li><a href="#">اخبار</a></li>
                <li><a href="#">سوابق نرخ ارز</a></li>
                <li><a href="#">تماس با ما</a></li>
                <li><a href="cooperate.html">همکاری با ما</a></li>
            </ul>
            <div class="logo" style="width:80px;float:left;">
                <img src="Main_pages/img/sama.png" width="100%" alt="">
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-collapse -->
</nav>