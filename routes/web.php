<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('main_page');
Route::get('/currency', 'IndexController@currency')->name('currency_page');
Route::get('/about', 'IndexController@about')->name('about_page');

Route::get('/page/{arg}', 'PageController@index');

Route::get('/news', 'NewsController@index');

Route::get('/plot/{arg}', 'ChartController@index');
Route::get('/history', 'ChartController@history');
Route::post('/plot/{arg}', 'ChartController@plot');
Route::get('/chart/iframe/{width}x{height}/{json}', 'ChartController@iframe')->where('json', '(.*)');;

Route::get('/contact-us', 'ContactUsController@index')->name('contact_page');
Route::post('/contact-us/submit', 'ContactUsController@submit');

Route::get('/cooperate', 'CooperateController@index')->name('cooperate_page');
Route::post('/cooperate/submit', 'CooperateController@submit')->name('cooperate_page');

Route::get('/convert', 'ConvertController@index');
Route::post('/convert', 'ConvertController@convert');

Route::post('/newsletter/submit', 'NewsletterController@submit');


require base_path('routes/dashboard.php');
