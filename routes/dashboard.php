<?php


Route::get('/logout', 'Auth\LoginController@logout');
Auth::routes();
Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function () {

    Route::get('/', function () {
        return redirect()->action('Dashboard\ExchangeTableController@index');
    });

    Route::get('debug', 'DebugController@debug');
    Route::get('log', 'DebugController@log');
    Route::get('view', 'DebugController@view');

    Route::get('elfinder/tinymce', function () {
        return view('vendor.elfinder.tinymce4');
    });
    Route::get('elfinder/library', function () {
        return view('vendor.elfinder.library');
    });

    Route::group(['prefix' => 'currency'], function () {
        Route::get('/', 'Dashboard\CurrencyController@index');
        Route::post('/submit', 'Dashboard\CurrencyController@submit');
    });

    Route::group(['prefix' => 'page'], function () {
        Route::get('/', 'Dashboard\PageController@index');
        Route::get('/single/{id?}', 'Dashboard\PageController@single');
        Route::post('/submit/{id?}', 'Dashboard\PageController@submit');
        Route::delete('/delete/{id}', 'Dashboard\PageController@delete');
    });

    Route::group(['prefix' => 'exchange-table'], function () {
        Route::get('/', 'Dashboard\ExchangeTableController@index');
        Route::get('/single/{id?}', 'Dashboard\ExchangeTableController@single');
        Route::post('/submit/{id?}', 'Dashboard\ExchangeTableController@submit');
        Route::delete('/delete/{id}', 'Dashboard\ExchangeTableController@delete');
    });

    Route::group(['prefix' => 'exchange-rate'], function () {
        Route::get('/', 'Dashboard\ExchangeRateController@index');
    });

    Route::group(['prefix' => 'newsletter'], function () {
        Route::get('/', 'Dashboard\NewsletterController@index');
    });

    Route::group(['prefix' => 'exchange-rate-manual'], function () {
        Route::get('/', 'Dashboard\ExchangeRateManualController@index');
        Route::post('/request-webservice', 'Dashboard\ExchangeRateManualController@requestWebservice');
        Route::get('/single/{id?}', 'Dashboard\ExchangeRateManualController@single');
        Route::post('/submit/{id?}', 'Dashboard\ExchangeRateManualController@submit');
        Route::delete('/delete/{id}', 'Dashboard\ExchangeRateManualController@delete');
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('/', 'Dashboard\MenuController@index');
        Route::get('/single/{id?}', 'Dashboard\MenuController@single');
        Route::post('/submit/{id?}', 'Dashboard\MenuController@submit');
        Route::delete('/delete/{id}', 'Dashboard\MenuController@delete');
    });

    Route::group(['prefix' => 'contact-us'], function () {
        Route::get('/', 'Dashboard\ContactUsController@index');
        Route::delete('/delete/{id}', 'Dashboard\ContactUsController@delete');
    });

    Route::group(['prefix' => 'cooperate'], function () {
        Route::get('/', 'Dashboard\CooperateController@index');
        Route::get('/download/{id}', 'Dashboard\CooperateController@download');
        Route::delete('/delete/{id}', 'Dashboard\CooperateController@delete');
    });

    Route::group(['prefix' => 'slider'], function () {
        Route::get('/', 'Dashboard\SliderController@index');
        Route::get('/single/{id?}', 'Dashboard\SliderController@single');
        Route::post('/submit/{id?}', 'Dashboard\SliderController@submit');
        Route::delete('/delete/{id}', 'Dashboard\SliderController@delete');
    });

    Route::group(['prefix' => 'file'], function () {
        Route::get('/', 'Dashboard\FileController@index');
    });

    Route::group(['prefix' => 'values'], function () {
        Route::get('/', 'Dashboard\ValuesController@index');
        Route::post('/', 'Dashboard\ValuesController@submit');
    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'Dashboard\ProfileController@getProfile');
        Route::post('/', 'Dashboard\ProfileController@postProfile');
        Route::get('/password', 'Dashboard\ProfileController@getPassword');
        Route::post('/password', 'Dashboard\ProfileController@postPassword');
    });
});