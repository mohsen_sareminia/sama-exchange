<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');

Route::group(['middleware' => 'api_token'], function () {
    Route::post('daily', 'Api\ExchangeRateController@daily');
    Route::post('detail', 'Api\ExchangeRateController@detail');
    Route::post('fresh', 'Api\ExchangeRateController@fresh');
    Route::post('convert', 'ConvertController@convert');
    Route::post('news', 'NewsController@table');
});
