<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('');
            $table->boolean('active')->default(true);
            $table->integer('order')->default(0);
            $table->text('data')->default(null);
            $table->text('columns')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_tables');
    }
}
