<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeRateManualsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_rate_manuals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency')->index();
            $table->float('rate')->index();
            $table->float('rate_buy')->index();
            $table->dateTime('date_from')->nullable()->index();
            $table->dateTime('date_to')->nullable()->index();
            $table->boolean('permanent')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_rate_manuals');
    }
}
