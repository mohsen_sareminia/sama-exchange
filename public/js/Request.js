var Request = {
    http: function (url, data) {
        url = url[0] == '/' ? url : '/' + url;
        data = data == undefined ? {} : data;
        return new Promise(function (resolve, reject) {
            $.ajax({
                type: "POST",
                url: HOST + url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: data,
                dataType: "JSON",
                success: function (res) {
                    resolve(res);
                },
                error: function (xhr, textStatus, errorThrown) {
                    reject(xhr, textStatus, errorThrown);
                }
            });
        });
    }
};