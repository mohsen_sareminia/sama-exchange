var Home = {};
$(document).ready(function () {
    Home = {
        setting: ['usd', 'eur', 'aed'],
        popup: {
            generateHtmlSelect: function (currency) {
                var currencyJson = JSON.parse($('#currency-json').html());
                var html = '<label>واحد پول :</label>' +
                    '<div class="form-group">' +
                    '<select class="form-control currency-select">';
                $.each(currencyJson, function (index, value) {
                    html += '<option value="' + index + '" ' + (currency != undefined && index == currency ? 'selected' : '') + '>' + value + '</option>';
                });
                html += '</select>' +
                    '</div>';

                return html;
            },
            generateHtmlForm: function () {
                var html = '';
                html = '<div class="form">';
                for (var i = 0; i < Home.setting.length; i++) {
                    html += this.generateHtmlSelect(Home.setting[i]);
                }
                html += '</div>';

                return html;
            },
            addSelect: function () {
                if ($('.currency-select').length < 10) {
                    $('#chart-setting-modal .modal-body .form').append(this.generateHtmlSelect());
                }
                else {
                    $('#chart-setting-modal .add-select-button').hide();
                }
            },
            submit: function () {
                var data = [];
                $.each($('.currency-select'), function (index, value) {
                    data.push($(value).val());
                });

                Request.http('dashboard/chart/chart', {data: data}).then(function (res) {
                    console.log(res);
                    if (res.success) {
                        $('#chart-setting-modal').modal('hide');
                    }
                });
            }
        }
    };
    $('#chart-setting-modal .modal-body').html(Home.popup.generateHtmlForm());
});