var Library = {
    current: undefined,
    open: function (name) {
        this.current = name;
        $('#library').modal('show');
    },
    select: function (file) {
        if (this.current != undefined) {
            $('input[name="' + Library.current + '"]').val(file.path);
            $('input[name="' + Library.current + '"]').parents('.input-group').next('.library-input').find('.library-input-image').html('<img src="' + HOST + '/' + file.path + '"/>');
        }
        else {
            $('input[name="' + Library.current + '"]').val('');
            $('input[name="' + Library.current + '"]').parents('.input-group').next('.library-input').find('.library-input-image').html('');
        }
        $('#library').modal('hide');
    }
};

$('#library').on('hidden.bs.modal', function () {
    Library.current = undefined;
});