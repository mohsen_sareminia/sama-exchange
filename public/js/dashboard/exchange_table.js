$(document).ready(function () {
    Table.init().draw();
});

var Table = {
    data: undefined,
    init: function () {
        this.data = JSON.parse($('#json').html());
        console.log(this.data);
        return this;
    },
    draw: function () {
        if (this.data != undefined && this.data.columns != undefined && this.data.rows != undefined) {
            console.log(this.data);
            var this_ = this;
            var html = '';
            html += '<tbody>';
            html += '<tr>';
            for (var i = 0; i < this.data.columns.length; i++) {
                var col = this.data.columns[i];
                html += '<td>';
                html += '<input type="text" class="form-control" value="' + col.value + '"  onkeyup="Table.setColumnValue(\'' + col.id + '\',this)" />';
                html += '<input type="checkbox" ' + (col.active ? 'checked' : '') + '  onchange="Table.setColumnActive(\'' + col.id + '\',this)" /><br/>';
                html += '<i class="fa fa-times" onclick="Table.delCol(\'' + col.id + '\')"></i>';
                html += '</td>'
            }
            html += '<td></td><td></td>';
            html += '</tr>';
            $.each(this.data.rows, function (index, row) {
                html += '<tr>';
                for (var i = 0; i < this_.data.columns.length; i++) {
                    var col = this_.data.columns[i];
                    var value = this_.findRowValue(row.values, col.id);
                    if (value != undefined) {
                        if (row.type == 'custom') {
                            html += '<td>';
                            html += '<input type="text" class="form-control" value="' + value.value + '" onkeyup="Table.setValue(\'' + col.id + '\',\'' + index + '\',this)" />';
                            html += '</td>'
                        }
                        else {
                            html += '<td>';
                            var select = $('#currency-input').html();
                            if (row.type != undefined) {
                                select = select.replace('value="' + value.value + '"', 'value="' + value.value + '" selected');
                                select = select.replace('<select', '<select onchange="Table.setValue(\'' + col.id + '\',\'' + index + '\',this)"');
                            }
                            html += select;
                            html += '</td>';
                        }
                    }
                }
                html += '<td>';
                var select = $('#type-input').html();
                if (row.type != undefined) {
                    select = select.replace('value="' + row.type + '"', 'value="' + row.type + '" selected');
                    select = select.replace('<select', '<select onchange="Table.changeType(this,\'' + index + '\')"');
                }
                html += select;
                html += '</td>';
                html += '<td><i class="fa fa-times" onclick="Table.delRow(\'' + index + '\')"></i><i class="fa fa-files-o" onclick="Table.cloneRow(\'' + index + '\')"></i></td>';
                html += '</tr>';
            });
            html += '</tbody>';

            $('#output').html(html);
            $('input[name="data"]').val(JSON.stringify(this.data));
        }
    },
    findRowValue: function (values, key) {
        for (var i = 0; i < values.length; i++) {
            if (values[i].id == key) {
                return values[i];
            }
        }
        return undefined;
    },
    delRowValue: function (values, key) {
        for (var i = 0; i < values.length; i++) {
            if (values[i].id == key) {
                values.splice(i, 1);
                return;
            }
        }
    },
    addCol: function () {
        var id = randomText();
        this.data.columns.push({
            id: id,
            active: true,
            value: ''
        });
        for (var i = 0; i < this.data.rows.length; i++) {
            this.data.rows[i].values.push({
                id: id,
                value: ''
            });
        }
        this.draw();
    },
    addRow: function () {
        var values = [];
        for (var i = 0; i < this.data.columns.length; i++) {
            var col = this.data.columns[i];
            values.push({
                id: col.id,
                value: ''
            });
        }
        this.data.rows.push({
            type: 'custom',
            values: values
        });
        this.draw();
    },
    delCol: function (id) {
        for (var i = 0; i < this.data.columns.length; i++) {
            var col = this.data.columns[i];
            if (col.id == id) {
                for (var j = 0; j < this.data.rows.length; j++) {
                    this.delRowValue(this.data.rows[j].values, col.id);
                }
                this.data.columns.splice(i, 1);
            }
        }
        this.draw();
    },
    delRow: function (index) {
        for (var j = 0; j < this.data.rows.length; j++) {
            if (j == index) {
                this.data.rows.splice(j, 1);
                break;
            }
        }
        this.draw();
    },
    cloneRow: function (index) {
        for (var j = 0; j < this.data.rows.length; j++) {
            if (j == index) {
                this.data.rows.splice(j, 0, jQuery.extend(true, {}, this.data.rows[j]));
                break;
            }
        }
        this.draw();
    },
    changeType: function (select, index) {
        for (var j = 0; j < this.data.rows.length; j++) {
            if (j == index) {
                this.data.rows[j].type = select.value;
                break;
            }
        }
        this.draw();
    },
    setValue: function (col_id, row_index, v) {
        var row = this.data.rows[row_index];
        if (row != undefined) {
            var value = this.findRowValue(row.values, col_id);
            if (value != undefined) {
                value.value = v.value;
            }
        }
        $('input[name="data"]').val(JSON.stringify(this.data));
    },
    setColumnValue: function (col_id, v) {
        for (var j = 0; j < this.data.columns.length; j++) {
            if (this.data.columns[j].id == col_id) {
                this.data.columns[j].value = v.value;
                break;
            }
        }
        $('input[name="data"]').val(JSON.stringify(this.data));
    },
    setColumnActive: function (col_id, v) {
        for (var j = 0; j < this.data.columns.length; j++) {
            if (this.data.columns[j].id == col_id) {
                this.data.columns[j].active = v.checked;
                break;
            }
        }
        $('input[name="data"]').val(JSON.stringify(this.data));
    }
};