var Toast = {
    error: function (errors) {
        var array = this.makeArray(errors);
        $.toast({
            heading: '',
            text: array,
            icon: 'error',
            position: 'top-right',
            textAlign: 'right',
            bgColor: '#d50000',
            loader: false,
            allowToastClose: false
        })
    },
    custom: function (text, icon, bg) {
        icon = icon == undefined ? "info" : icon;
        bg = bg == undefined ? "#00b24a" : bg;
        $.toast({
            heading: '',
            text: text,
            icon: icon,
            position: 'top-right',
            textAlign: 'right',
            bgColor: bg,
            loader: false,
            allowToastClose: false
        })
    },
    makeArray: function (errors) {
        var result = [];
        $.each(errors, function (index, value) {
            $.each(value, function (index2, value2) {
                result.push(value2);
            })
        });

        return result;
    }
};