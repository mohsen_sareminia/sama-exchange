$(document).ready(function () {
    Chart.defaults.global.defaultFontFamily = "primary_font";

    $.each($('.datetimepicker'), function (index, value) {
        $(value).MdPersianDateTimePicker({
            TargetSelector: '#' + $(value).attr('id'),
            Trigger: 'click',
            EnableTimePicker: false,
            Format: 'yyyy-MM-dd',
            EnglishNumber: true
        });
    });

    var currency = $('meta[name="currency"]').attr('content');
    var currency_fa = $('meta[name="currency-fa"]').attr('content');
    if (currency != undefined && currency_fa != undefined) {
        Request.http('plot/' + currency).then(function (res) {
            if (res.success) {
                var labels = [];
                var rates_sell = [];
                var rates_buy = [];
                $.each(res.data, function (index, value) {
                    labels.push(value.date_jalali);
                    rates_sell.push(value.rate);
                    rates_buy.push(value.rate_buy);
                });
                plot(currency, currency_fa, labels, rates_sell, rates_buy);
            }
        });
    }
    else {
        search();
    }
});

function search() {
    var currency = $('#currency').val();
    var data = {
        sell: $('#sell').is(':checked'),
        buy: $('#buy').is(':checked'),
        date_from: $('#date_from').val(),
        date_to: $('#date_to').val()
    };
    Request.http('plot/' + currency, data).then(function (res) {
        if (res.success) {
            var labels = [];
            var rates_sell = [];
            var rates_buy = [];
            $.each(res.data, function (index, value) {
                labels.push(value.date_jalali);
                if (data.sell) {
                    rates_sell.push(value.rate);
                }
                if (data.buy) {
                    rates_buy.push(value.rate_buy);
                }
            });
            plot(res.currency, res.currency_fa, labels, rates_sell, rates_buy);
        }
        else {
            Toast.error(res.errors);
        }
    });
}

var chart = undefined;
function plot(currency, currency_fa, labels, rates_sell, rates_buy) {
    var ctx = document.getElementById('currency-chart').getContext('2d');
    document.getElementById('currency-chart').height = 450;
    document.getElementById('currency-chart').width = $('#currency-chart').parent().width();

    var datasets = [];
    if (rates_sell != undefined && rates_sell.length > 0) {
        datasets.push({
            label: 'نرخ ' + currency_fa,
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: rates_sell,
            fill: false
        });
    }
    // if (rates_buy != undefined && rates_buy.length > 0) {
    //     datasets.push({
    //         label: 'نرخ خرید ' + currency_fa,
    //         backgroundColor: 'rgb(84, 135, 255)',
    //         borderColor: 'rgb(84, 135, 255)',
    //         data: rates_buy,
    //         fill: false
    //     });
    // }

    if (chart != undefined) {
        chart.destroy();
    }
    chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: datasets
        },

        // Configuration options go here
        options: {
            responsive: false,
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }
        }
    });
}