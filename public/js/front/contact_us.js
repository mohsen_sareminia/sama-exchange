$('.btn-submit').click(function () {
    var this_ = this;
    $(this_).attr('disabled', true);
    Request.http('contact-us/submit', {
        name: $('#name').val(),
        email: $('#email').val(),
        message: $('#message').val()
    }).then(function (res) {
        if (res.success) {
            Toast.custom('پیام شما با موفیت ثبت شد');
            $('#name').val('');
            $('#email').val('');
            $('#message').val('');
        }
        else {
            Toast.error(res.errors);
        }
        $(this_).removeAttr('disabled');
    });
});