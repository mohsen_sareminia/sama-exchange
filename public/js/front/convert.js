function search() {
    var data = {
        source_rate: $('#source').val(),
        destination_rate: $('#destination').val(),
        value: $('#value').val()
    };
    Request.http('convert', data).then(function (res) {
        if (res.success) {
            var html = '';
            html += '<table class="table table-bordered table-hover text-center">';
            html += '<tbody>';
            html += '<tr>';
            html += '<td>مبلغ تبدیل شده</td><td>' + toPersianNum(res.value) + '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>نسبت</td><td>' + toPersianNum(res.ratio) + '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>نرخ پایه به ریال</td><td>' + toPersianNum(res.source) + '</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td>نرخ هدف به ریال</td><td>' + toPersianNum(res.destination) + '</td>';
            html += '</tr>';
            html += '</tbody>';
            html += '</table>';

            $('.result').html(html);
        }
        else {
            Toast.error(res.errors);
        }
    });
}