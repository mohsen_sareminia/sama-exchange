$(document).ready(function () {
    $("#home-slider").lightSlider({
        item: 1,
        loop: true,
        keyPress: true,
        auto: true,
        pauseOnHover: true,
        pager: false,
        speed: 1500,
        pause: 5000,
        cssEasing: 'cubic-bezier(0.075, 0.82, 0.165, 1)',
        prevHtml: '<i class="fa fa-angle-left"></i>',
        nextHtml: '<i class="fa fa-angle-right"></i>'
    });

    try {
        var ratio = JSON.parse($('#ratio-json').html());
        Chart.defaults.global.defaultFontFamily = "primary_font";
        var ctx = document.getElementById('currency-chart').getContext('2d');
        var h = $('.slider-card').height() - $('.social-network').height();
        document.getElementById('currency-chart').height = h < 200 ? 200 : h;
        document.getElementById('currency-chart').width = $('#currency-chart').parent().width();

        var labels = [];
        var rates_sell = [];
        var rates_buy = [];
        $.each(ratio, function (index, value) {
            labels.push(value.date_jalali);
            rates_sell.push(value.rate);
            rates_buy.push(value.rate_buy);
        });

        var datasets = [];
        datasets.push({
            label: 'نرخ تبدیل دلار به یورو',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: rates_sell,
            fill: false
        });
        // datasets.push({
        //     label: '  نرخ خرید دلار به یورو',
        //     backgroundColor: 'rgb(84, 135, 255)',
        //     borderColor: 'rgb(84, 135, 255)',
        //     data: rates_buy,
        //     fill: false
        // });

        new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: datasets
            },
            options: {
                responsive: false,
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                }
            }
        });
    } catch (e) {
        console.log(e);
    }

});