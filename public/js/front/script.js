$(document).ready(function () {
    initClock();
});

function initClock() {
    if ($('.clocks').is(':visible')) {
        for (var i = 1; i <= 6; i++) {
            var id = "clock-canvas-" + i;
            var diff = parseInt($('#' + id).attr('data-diff'));
            var myClock = new clock(id, {
                addSeconds: diff,
                colour: "#a8a8a8",
                rimColour: "black",
                markerColour: '#4b4b4b',
                rim: 1,
                markerType: "dot"
            });
            myClock.start();
        }
    }
}

function randomText() {
    var result = "";
    var possible = "qweSDFGHJKLWERDTFGYHsdfgh" +
        "jnkSDFGHJKLsdfghjkSDFGHJKsdFGHJSDXFH" +
        "JsdfghjSDFGHJKsdfghjWERTFYU" +
        "IOPXCVBNMPOLKJRDAOIJKNMRDF" +
        "VEWSDZRIKJrfdcvtgfikjoklrty" +
        "uiopsdfghjklzxcvbnmasdfghjk" +
        "qwertyuiosdfghjxcvbnmSADFGH" +
        "JKWERTYOXCVBNSDFGHJ";

    for (var i = 0; i < 10; i++) {
        result += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return result;
}

function toPersianNum(num, dontTrim) {

    var i = 0,

        dontTrim = dontTrim || false,

        num = dontTrim ? num.toString() : num.toString().trim(),
        len = num.length,

        res = '',
        pos,

        persianNumbers = typeof persianNumber == 'undefined' ?
            ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'] :
            persianNumbers;

    for (; i < len; i++)
        if (( pos = persianNumbers[num.charAt(i)] ))
            res += pos;
        else
            res += num.charAt(i);

    return res;
}

$(document).on('click', '.submit-newsletter', function () {
    var this_ = this;
    $(this_).attr('disabled', true);

    Request.http('newsletter/submit', {
        email: $('#newsletter-email').val()
    }).then(function (res) {
        if (res.success) {
            Toast.custom('ایمیل شما با موفیت ثبت شد');
            $('#newsletter-email').val('');
        } else {
            Toast.error(res.errors);
        }
        $(this_).removeAttr('disabled');
    });
});