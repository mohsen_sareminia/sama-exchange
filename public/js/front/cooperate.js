$('.btn-submit').click(function () {
    var this_ = this;
    $(this_).attr('disabled', true);

    try {
        var data = new FormData();
        data.append('rezome', $('#rezome')[0].files[0]);
        data.append('first_name', $('#first_name').val());
        data.append('last_name', $('#last_name').val());
        data.append('phone', $('#phone').val());
        data.append('email', $('#email').val());
        data.append('description', $('#description').val());
        data.append('salary', $('#salary').val());

        $.ajax({
            type: "POST",
            method: "POST",
            url: HOST + '/cooperate/submit',
            contentType: false,
            processData: false,
            async:false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            dataType: 'json',
            success: function (res) {
                if (res.success) {
                    Toast.custom('پیام شما با موفیت ثبت شد');
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#phone').val('');
                    $('#email').val('');
                    $('#description').val('');
                    $('#salary').val('');
                    $('#rezome').val('');
                }
                else {
                    Toast.error(res.errors);
                }
                $(this_).removeAttr('disabled');
            }
        });
    }
    catch (e) {
        $(this_).removeAttr('disabled');
    }


});