function initTinyMCE() {

    //TinyMCE
    tinymce.init({
        selector: "textarea.tinymce",
        forced_root_block: "",
        theme: "modern",
        height: 300,
        directionality: "rtl",

        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,
        allow_script_urls: true,

        autosave_ask_before_unload: false,
        content_css: HOST + '/plugin/tinymce/style.css',
        toolbar: "rtl",
        fontsize_formats: "8px 10px 12px 14px 16px 18px 20px 22px 24px 26px 28px 36px 72px",
        plugins: [
            'advlist directionality autolink autosave lists  link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | library | h1 h2 h3 h4 | p | fullpage',
        file_browser_callback: elFinderBrowser,
        setup: function (editor) {
            editor.addButton('h1',
                {
                    title: 'h1',
                    text: 'h1',
                    onclick: function () {
                        var c = editor.selection.getContent({format: "text"});
                        var className = c == "" ? "tinymce-empty-block" : "";
                        editor.selection.setContent('<h1 class="' + className + '">' + c + '</h1>');
                    }
                });
            editor.addButton('h2',
                {
                    title: 'h2',
                    text: 'h2',
                    onclick: function () {
                        var c = editor.selection.getContent({format: "text"});
                        var className = c == "" ? "tinymce-empty-block" : "";
                        editor.selection.setContent('<h2 class="' + className + '">' + c + '</h2>');
                    }
                });
            editor.addButton('h3',
                {
                    title: 'h3',
                    text: 'h3',
                    onclick: function () {
                        var c = editor.selection.getContent({format: "text"});
                        var className = c == "" ? "tinymce-empty-block" : "";
                        editor.selection.setContent('<h3 class="' + className + '">' + c + '</h3>');
                    }
                });
            editor.addButton('h4',
                {
                    title: 'h4',
                    text: 'h4',
                    onclick: function () {
                        var c = editor.selection.getContent({format: "text"});
                        var className = c == "" ? "tinymce-empty-block" : "";
                        editor.selection.setContent('<h4 class="' + className + '">' + c + '</h4>');
                    }
                });
            editor.addButton('p',
                {
                    title: 'p',
                    text: 'p',
                    onclick: function () {
                        var c = editor.selection.getContent({format: "text"});
                        var className = c == "" ? "tinymce-empty-block" : "";
                        editor.selection.setContent('<p class="' + className + '">' + c + '</p>');
                    }
                });
        }
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = HOST + '/plugin/tinymce';
    tinymce.DOM.addClass('table', 'bordered  striped highlight');

    function elFinderBrowser(field_name, url, type, win) {
        tinymce.activeEditor.windowManager.open({
            file: HOST + '/dashboard/elfinder/tinymce',// use an absolute path!
            title: 'elFinder 2.0',
            width: 900,
            height: 450,
            resizable: 'yes'
        }, {
            setUrl: function (file) {
                win.document.getElementById(field_name).value = file.url;
            }
        });
        return false;
    }
};